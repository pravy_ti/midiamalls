# Admin BrMalls - pradrão Laravel

Versão do PHP: 7.2
Versão do MySql: 5.7.32

# Instruções para instalação
## Composer

Instala as dependências e bibliotecas do PHP
```
composer install
```

## NPM
Instala as dependências e bibliotecas para o front-end
```
npm install
```

Executa o projeto e gera build (css e js)
```
npm run watch + php artisan serve
```

## .Env
Arquivo de configuração da aplicação
Copiar o arquivo com o seguinte comando:
```
cp .env.example .env
```
Verificar as configurações de banco de dados e demais serviços


## Banco de dados

Todos os bancos de dados são MySQl
* Não rodar as migrations para criação das tabelas - não funciona.
* Fazer o dump do banco de dados (usando as credenciais fornecidas) e usar ele como massa de dados localmente
* Só é possível acessar os bancos e os servidores via VPN

## Configurações adicionais
chmod 775 -R storage
php artisan storage:link


## Acesso ao admin
helder@pravy.com.br
123mkt456

## Cache
Usar somente em produção
```
php artisan config:cache
```

## Front-end
Blade do laravel + JS/Jquery + Vue
