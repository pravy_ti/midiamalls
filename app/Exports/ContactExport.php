<?php

namespace App\Exports;

use App\Models\Contact;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithCustomCsvSettings;

class ContactExport implements FromCollection, WithHeadings, WithCustomCsvSettings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Contact::all();
    }

        public function getCsvSettings(): array
    {
        return [
            'delimiter' => ';'
         ];
    }
    public function headings(): array
    {
        return [
            '#',
            'Nome',
            'Email',
            'Mensagem',
            'Enviado as',
        ];
    }

}
