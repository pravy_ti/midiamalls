<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use \DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Mail;
use App\Models\Contact;
use App\Mail\ConfirmaContato;
use App\Mail\ConfirmaContatoAdmin;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $banner_con = DB::table('banners')->select('banners.*')->where('pagename','fale')->first();
        $contact = DB::table('pages')->select('pages.*')->where('pagename','fale')->first();
        $banner = DB::table('banners')->select('banners.*')->where('pagename','home')->first();
        $page = DB::table('pages')->select('pages.*')->where('pagename','home')->first();
        $footer = DB::table('footer')->select('footer.*')->where('id','1')->first();
        $announce = DB::table('announce')->select('announce.*')->get();
        $propriedades = DB::table('properties')->select('properties.*')->where('id_shopping','0')->get();
        $shoppings = DB::table('shoppings')->select('shoppings.*')->orderby('destaque','desc')->get();
        $SEO_vars = [
            'page_title' => 'MidiaMALLS - Midia OOH em Shoppings centers',
            'description' => 'A mídiaMALLS leva o OOH para os principais centros de compras em todo o Brasil. Saiba onde estamos presentes e anuncie com a gente!',
        ];

        $pro1 = DB::table('properties')->select('properties.*')->where('id_shopping','0')->count();

        return view('welcome', compact('banner','page','footer','announce','contact', 'banner_con','propriedades','shoppings','pro1','SEO_vars'));
    }


        public function faleconosco(Request $request)
    {
        $dados = $request->all();

        $contato = DB::table('pages')->where('pagename','fale')->select('pages.*')->first();

        $nome  = $request->get('name');
        $email = $request->get('email');
        $mensagem  = $request->get('message');

        $fale  = New Contact;
        $fale->name = $dados['name'];
        $fale->email = $dados['email'];
        $fale->message = $dados['message'];


        $emails = explode(';', $contato->title);

        foreach($emails as $send_email){
            $att= DB::table('contacts')->insert(['name' => ($nome), 'mail' => ($email), 'message'=> ($mensagem), 'created_at' => \Carbon\Carbon::now()]);

            $title = "midiaMalls - Fale Conosco";

            //$send_email = ' amystis@gmail.com ';

            Mail::to(trim($send_email),'midiaMalls - Fale Conosco')->send(new ConfirmaContatoAdmin($fale));

        }

        $return['error'] = 0;
        $return['msg'] = 'Mensagem enviada com sucesso!';

        return json_encode($return);
        //return redirect('/')->with('status','Mensagem Enviada');


    }
}
