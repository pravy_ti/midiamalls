<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use \DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class MidiaController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
  /*  public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $banner = DB::table('banners')->select('banners.*')->where('pagename','midia')->first();
        $page = DB::table('pages')->select('pages.*')->where('pagename','midia')->first();
        $footer = DB::table('footer')->select('footer.*')->where('id','1')->first();
        
        return view('about', compact('banner','page','footer'));
    }
    
    
}
