<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use \DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class PropriedadesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
   /* public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $banner = DB::table('banners')->select('banners.*')->where('pagename','propriedades')->first();
        $page = DB::table('pages')->select('pages.*')->where('pagename','propriedades')->first();
        $footer = DB::table('footer')->select('footer.*')->where('id','1')->first();
        $shoppings = DB::table('shoppings')->select('shoppings.*')->get();
        $propriedades = DB::table('properties')->select('properties.*')->where('id_shopping','0')->get();
            $shopro = DB::table('properties')
        ->join('shoppings','properties.id_shopping','=','shoppings.id')
        ->select('properties.*','properties.id as idprop','shoppings.id')->get();
        
        
        return view('propriedades', compact('banner','page','footer','shoppings','propriedades','shopro'));
    }
}
