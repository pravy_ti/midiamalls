<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use \DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class ShoppingsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
   /* public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $banner = DB::table('banners')->select('banners.*')->where('pagename','shoppings')->first();
        $page = DB::table('pages')->select('pages.*')->where('pagename','shoppings')->first();
        $footer = DB::table('footer')->select('footer.*')->where('id','1')->first();
        //$shoppings = DB::table('shoppings')->select('shoppings.*')->orderBy('destaque', 'desc')->get();
        $shoppings = DB::table('shoppings')->select('shoppings.*')->orderBy('name', 'asc')->get();
        $SEO_vars = [
            'page_title' => 'Shoppings por todo o Brasil – MidiaMALLS',
            'description' => 'Presente nas cinco regiões do Brasil e onde seu público estiver. Conheça os shopping centers em que estamos presentes e deixe sua marca!',
        ];

        return view('nossos-shoppings', compact('banner','page','footer','shoppings','SEO_vars'));
    }
}
