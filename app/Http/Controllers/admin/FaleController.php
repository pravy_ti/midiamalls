<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use \DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use App\Models\Contact;
use App\Exports\ContactExport;
use Maatwebsite\Excel\Facades\Excel;

class FaleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $banner = DB::table('banners')->select('banners.*')->where('pagename','fale')->first();
        $page = DB::table('pages')->select('pages.*')->where('pagename','fale')->first();
        
        return view('admin.fale', compact('banner','page'));

    }
    
    
    
        public function update(Request $request){
        $page   = 'fale'; 
            
        $email             =$request->get('mail');
        $contato           =$request->get('phone');
        $endereco          =$request->get('address');
        $endereco_link     =$request->get('address_link');
        

        
        
            
        //Banners

        if($request->hasFile('banner_desktop') && $request->file('banner_desktop')->isValid()) {
        $name = uniqid(date('HisYmd'));
        $extension = $request->banner_desktop->extension();
        $nameFile = "{$name}.{$extension}";
        $upload = $request->banner_desktop->storeAs('public/uploads/banners/', $nameFile, 'azure');

$att =     DB::table('banners')->where('pagename',($page))->update(['desktop' => ($nameFile)]);
}   
            
      

        //Textos de Conteúdo
            DB::table('pages')->where(['pagename'=>($page)])->update([
            'title' => ($email),
            'text' => ($contato),
            'title2' => ($endereco_link),
            'text2' => ($endereco),
            'updated_at' => \Carbon\Carbon::now()
        ]);
            

        return redirect('admin/fale-conosco')->with('alert', 'Fale Conosco Atualizado!');

    }
    
        public function export()
    {
        return Excel::download(new ContactExport, 'contato.csv');
    }
}
