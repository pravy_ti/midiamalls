<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use \DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class FooterController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $footer = DB::table('footer')->select('footer.*')->where('id','1')->first();
        
        return view('admin.footer',compact('footer'));
    }
    
      public function update(Request $request){
        $page   = 'footer'; 

        $address             =$request->get('adress');
        $address_link        =$request->get('adress_link');
        $contact_type        =$request->get('tel_type');
        $contact_number      =$request->get('tel_number');
        $contact_type2       =$request->get('tel_type2');
        $contact_number2     =$request->get('tel_number2');
        $contact_type3       =$request->get('tel_type3');
        $contact_number3     =$request->get('tel_number3');
        $contact_type4       =$request->get('tel_type4');
        $contact_number4     =$request->get('tel_number4');
        $contact_type5       =$request->get('tel_type5');
        $contact_number5     =$request->get('tel_number5');
          
        $company             =$request->get('company');
        $company_name        =$request->get('company_name');
        $company_mail        =$request->get('company_mail');
        $company_number      =$request->get('company_number');
        $company_name2       =$request->get('company_name2');
        $company_mail2       =$request->get('company_mail2');
        $company_number2     =$request->get('company_number2');
          
        $instagram           =$request->get('instagram');
        $facebook            =$request->get('facebook');
        $linkedin            =$request->get('linkedin');
          
        $terms               =$request->get('terms');
        $policy              =$request->get('policy');

        
            
        //Logos

       if($request->hasFile('logo') && $request->file('logo')->isValid()) {
        $name = uniqid(date('HisYmd'));
        $extension = $request->logo->extension();
        $nameFile = "{$name}.{$extension}";
        $upload = $request->logo->storeAs('public/uploads/icons/', $nameFile, 'azure');

$att =     DB::table('footer')->where('id','1')->update(['logo' => ($nameFile)]);
}   
        if($request->hasFile('logo2') && $request->file('logo2')->isValid()) {
        $name = uniqid(date('HisYmd'));
        $extension = $request->logo2->extension();
        $nameFile = "{$name}.{$extension}";
        $upload = $request->logo2->storeAs('public/uploads/icons/', $nameFile, 'azure');

$att =     DB::table('footer')->where('id','1')->update(['company' => ($nameFile)]);
}  
        if($request->hasFile('logo3') && $request->file('logo3')->isValid()) {
        $name = uniqid(date('HisYmd'));
        $extension = $request->logo3->extension();
        $nameFile = "{$name}.{$extension}";
        $upload = $request->logo3->storeAs('public/uploads/icons/', $nameFile,'azure');

$att =     DB::table('footer')->where('id','1')->update(['company2' => ($nameFile)]);
}  
          
        if($request->hasFile('termos') && $request->file('termos')->isValid()) {
        $name = $request->termos->getClientOriginalName();
        $extension = $request->termos->extension();
        $nameFile = "{$name}";
        $upload = $request->termos->storeAs('public/uploads/files/', $nameFile, 'azure');

$att =     DB::table('footer')->where('id','1')->update(['terms' => ($nameFile)]);
}  
        if($request->hasFile('politicas') && $request->file('politicas')->isValid()) {
        $name = $request->politicas->getClientOriginalName();
        $extension = $request->politicas->extension();
        $nameFile = "{$name}";
        $upload = $request->politicas->storeAs('public/uploads/files/', $nameFile,'azure');

$att =     DB::table('footer')->where('id','1')->update(['policy' => ($nameFile)]);
}  
        
            

            
        //Textos de Conteúdo
            DB::table('footer')->where(['id'=>'1'])->update([
            'contact_adress'     => ($address),
            'contact_link'       => ($address_link),
            'phonetype'          => ($contact_type),
            'phonenumber'        => ($contact_number),
            'phonetype2'         => ($contact_type2),
            'phonenumber2'       => ($contact_number2), 
            'phonetype3'         => ($contact_type3),
            'phonenumber3'       => ($contact_number3), 
            'phonetype4'         => ($contact_type4),
            'phonenumber4'       => ($contact_number4), 
            'phonetype5'         => ($contact_type5),
            'phonenumber5'       => ($contact_number5), 
            'press_company'      => ($company),
            'press_name'         => ($company_name),
            'press_mail'         => ($company_mail),
            'press_phone'        => ($company_number),
            'press_name2'        => ($company_name2),
            'press_mail2'        => ($company_mail2),
            'press_phone2'       => ($company_number2),
            'linkedin'           => ($linkedin),
            'instagram'          => ($instagram),
            'facebook'           => ($facebook),
                
        ]);
          
            if($terms == 'on'){$att = DB::table('footer')->where(['id'=>'1'])->update(['terms' => null]);};
           if($policy == 'on'){$att = DB::table('footer')->where(['id'=>'1'])->update(['policy' => null]);};
            


        return redirect('admin/footer');

    }
}
