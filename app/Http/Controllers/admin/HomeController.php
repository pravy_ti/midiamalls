<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Validator;
use \DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $banner = DB::table('banners')->select('banners.*')->where('pagename', 'home')->first();
        $page = DB::table('pages')->select('pages.*')->where('pagename', 'home')->first();
        $announce = DB::table('announce')->select('announce.*')->where('id', '1')->first();
        $announce2 = DB::table('announce')->select('announce.*')->where('id', '2')->first();
        $announce3 = DB::table('announce')->select('announce.*')->where('id', '3')->first();
        $announce4 = DB::table('announce')->select('announce.*')->where('id', '4')->first();
        $announce5 = DB::table('announce')->select('announce.*')->where('id', '5')->first();
        $announce6 = DB::table('announce')->select('announce.*')->where('id', '6')->first();

        return view('admin.home', compact('page', 'announce', 'announce2', 'announce3', 'announce4', 'announce5', 'announce6', 'banner'));
    }

    public function update(Request $request)
    {
        $page = 'home';

        $dados = $request->all();
        $rules = array(
            'content_link6' => 'mimes:pdf|max:10240', //10mb  10240
            'banner_desktop' => 'image|max:2048', // 2mb
            'banner_mobile' => 'image|max:2048',
            'icon_file' => 'mimes:png|max:2048',
            'icon_file2' => 'mimes:png|max:2048',
            'icon_file3' => 'mimes:png|max:2048',
            'icon_file4' => 'mimes:png|max:2048',
            'icon_file5' => 'mimes:png|max:2048',
            'icon_file6' => 'mimes:png|max:2048',
            'midia1' => 'image|max:2048',
            'midia2' => 'image|max:2048',
            'midia3' => 'image|max:2048',
            'midia4' => 'image|max:2048',
        );

        $messages = array(
            'content_link6.max' => 'A Tabela de preços deve ter no máximo 10Mb.',
            'content_link6.mimes' => 'A Tabela de preços deve ser um arquivo PDF.',
            'banner_desktop.image' => 'Banner desktop deve ter o formato PNG ou JPG.',
            'banner_desktop.max' => 'Banner desktop deve ter no máximo 2Mb.',
            'banner_mobile.image' => 'Banner mobile deve ter o formato PNG ou JPG.',
            'banner_mobile.max' => 'Banner mobile deve ter no máximo 2Mb.',
            'icon_file.max' => 'Icone 1 deve ter no máximo 2Mb.',
            'icon_file.mimes' => 'Icone 1 deve ser um arquivo PNG.',
            'icon_file2.max' => 'Icone 2 deve ter no máximo 2Mb.',
            'icon_file2.mimes' => 'Icone 2 deve ser um arquivo PNG.',
            'icon_file3.max' => 'Icone 3 deve ter no máximo 2Mb.',
            'icon_file3.mimes' => 'Icone 3 deve ser um arquivo PNG.',
            'icon_file4.max' => 'Icone 4 deve ter no máximo 2Mb.',
            'icon_file4.mimes' => 'Icone 4 deve ser um arquivo PNG.',
            'icon_file5.max' => 'Icone 5 deve ter no máximo 2Mb.',
            'icon_file5.mimes' => 'Icone 5 deve ser um arquivo PNG.',
            'icon_file6.max' => 'Icone 6 deve ter no máximo 2Mb.',
            'icon_file6.mimes' => 'Icone 6 deve ser um arquivo PNG.',
            'midia1.image' => 'Imagem deve ter o formato PNG ou JPG.',
            'midia1.max' => 'Imagem deve ter no máximo 2Mb.',
            'midia2.image' => 'Imagem 2 deve ter o formato PNG ou JPG.',
            'midia2.max' => 'Imagem 2 deve ter no máximo 2Mb.',
            'midia3.image' => 'Imagem 3 deve ter o formato PNG ou JPG.',
            'midia3.max' => 'Imagem 3 deve ter no máximo 2Mb.',
            'midia4.image' => 'Imagem 4 deve ter o formato PNG ou JPG.',
            'midia4.max' => 'Imagem 4 deve ter no máximo 2Mb.',

        );

        $validator = Validator::make($dados, $rules, $messages);

        if ($validator->fails()) {
            return redirect('admin/home')->withErrors($validator)->withInput();
        }

        $banner_desktop_title = $request->get('banner_desk_title');
        $banner_mobile_title = $request->get('banner_mobile_title');
        $content_title = $request->get('content_title');
        $content_link = $request->get('content_link');
        $content_text = $request->get('content_text');
        $content_title2 = $request->get('content_title2');
        $content_text2 = $request->get('content_text2');
        $content_title3 = $request->get('content_title3');
        $content_link3 = $request->get('content_link3');
        $content_text3 = $request->get('content_text3');
        $content_title4 = $request->get('content_title4');
        $content_link4 = $request->get('content_link4');
        $content_text4 = $request->get('content_text4');
        $content_title5 = $request->get('content_title5');
        $content_link5 = $request->get('content_link5');
        $content_text5 = $request->get('content_text5');
        $content_title6 = $request->get('content_title6');
        $content_link6 = $request->get('content_link6');
        $content_text6 = $request->get('content_text6');
        $subtitle6 = $request->get('subtitle6');
        $midia_kit = $request->get('midia_kit');
        $midia_kit_completo = $request->get('midia_kit_completo');
        $icon_name = $request->get('icon_name');
        $icon_name2 = $request->get('icon_name2');
        $icon_name3 = $request->get('icon_name3');
        $icon_name4 = $request->get('icon_name4');
        $icon_name5 = $request->get('icon_name5');
        $icon_name6 = $request->get('icon_name6');

        //Icones

        if ($request->hasFile('icon_file') && $request->file('icon_file')->isValid()) {
            $name = uniqid(date('HisYmd'));
            $extension = $request->icon_file->extension();
            $nameFile = "{$name}.{$extension}";
            $upload = $request->icon_file->storeAs('public/uploads/icons/', $nameFile, 'azure');

            $att = DB::table('announce')->where('id', '1')->update(['icon' => ($nameFile)]);

        }
        if ($request->hasFile('icon_file2') && $request->file('icon_file2')->isValid()) {
            $name = uniqid(date('HisYmd'));
            $extension = $request->icon_file2->extension();
            $nameFile = "{$name}.{$extension}";
            $upload = $request->icon_file2->storeAs('public/uploads/icons/', $nameFile, 'azure');

            $att = DB::table('announce')->where('id', '2')->update(['icon' => ($nameFile)]);
        }
        if ($request->hasFile('icon_file3') && $request->file('icon_file3')->isValid()) {
            $name = uniqid(date('HisYmd'));
            $extension = $request->icon_file3->extension();
            $nameFile = "{$name}.{$extension}";
            $upload = $request->icon_file3->storeAs('public/uploads/icons/', $nameFile, 'azure');

            $att = DB::table('announce')->where('id', '3')->update(['icon' => ($nameFile)]);
        }
        if ($request->hasFile('icon_file4') && $request->file('icon_file4')->isValid()) {
            $name = uniqid(date('HisYmd'));
            $extension = $request->icon_file4->extension();
            $nameFile = "{$name}.{$extension}";
            $upload = $request->icon_file4->storeAs('public/uploads/icons/', $nameFile, 'azure');

            $att = DB::table('announce')->where('id', '4')->update(['icon' => ($nameFile)]);
        }
        if ($request->hasFile('icon_file5') && $request->file('icon_file5')->isValid()) {
            $name = uniqid(date('HisYmd'));
            $extension = $request->icon_file5->extension();
            $nameFile = "{$name}.{$extension}";
            $upload = $request->icon_file5->storeAs('public/uploads/icons/', $nameFile, 'azure');

            $att = DB::table('announce')->where('id', '5')->update(['icon' => ($nameFile)]);
        }
        if ($request->hasFile('icon_file6') && $request->file('icon_file6')->isValid()) {
            $name = uniqid(date('HisYmd'));
            $extension = $request->icon_file6->extension();
            $nameFile = "{$name}.{$extension}";
            $upload = $request->icon_file6->storeAs('public/uploads/icons/', $nameFile, 'azure');

            $att = DB::table('announce')->where('id', '6')->update(['icon' => ($nameFile)]);
        }

        //Banners

        if ($request->hasFile('banner_mobile') && $request->file('banner_mobile')->isValid()) {
            $name = uniqid(date('HisYmd'));
            $extension = $request->banner_mobile->extension();
            $nameFile2 = "{$name}.{$extension}";
            $upload = $request->banner_mobile->storeAs('public/uploads/banners/', $nameFile2, 'azure');

            $att = DB::table('banners')->where('pagename', ($page))->update(['mobile' => ($nameFile2)]);
        }
        if ($request->hasFile('banner_desktop') && $request->file('banner_desktop')->isValid()) {
            $name = uniqid(date('HisYmd'));
            $extension = $request->banner_desktop->extension();
            $nameFile = "{$name}.{$extension}";
            $upload = $request->banner_desktop->storeAs('public/uploads/banners/', $nameFile, 'azure');

            $att = DB::table('banners')->where('pagename', ($page))->update(['desktop' => ($nameFile)]);
        }

        //Midias

        if ($request->hasFile('midia1') && $request->file('midia1')->isValid()) {
            $name = uniqid(date('HisYmd'));
            $extension = $request->midia1->extension();
            $nameFile = "{$name}.{$extension}";
            $upload = $request->midia1->storeAs('public/uploads/pages/', $nameFile, 'azure');

            $att = DB::table('pages')->where('pagename', ($page))->update(['image1' => ($nameFile)]);
        }
        if ($request->hasFile('midia2') && $request->file('midia2')->isValid()) {
            $name = uniqid(date('HisYmd'));
            $extension = $request->midia2->extension();
            $nameFile = "{$name}.{$extension}";
            $upload = $request->midia2->storeAs('public/uploads/pages/', $nameFile, 'azure');

            $att = DB::table('pages')->where('pagename', ($page))->update(['image2' => ($nameFile)]);
        }
        if ($request->hasFile('midia3') && $request->file('midia3')->isValid()) {
            $name = uniqid(date('HisYmd'));
            $extension = $request->midia3->extension();
            $nameFile = "{$name}.{$extension}";
            $upload = $request->midia3->storeAs('public/uploads/pages/', $nameFile, 'azure');

            $att = DB::table('pages')->where('pagename', ($page))->update(['image3' => ($nameFile)]);
        }
        if ($request->hasFile('midia4') && $request->file('midia4')->isValid()) {
            $name = uniqid(date('HisYmd'));
            $extension = $request->midia4->extension();
            $nameFile = "{$name}.{$extension}";
            $upload = $request->midia4->storeAs('public/uploads/pages/', $nameFile, 'azure');

            $att = DB::table('pages')->where('pagename', ($page))->update(['image4' => ($nameFile)]);
        }

        //Tabela de preço
        if ($request->hasFile('content_link6') && $request->file('content_link6')->isValid()) {
            $name = 't' . uniqid(date('HisYmd'));
            $extension = $request->content_link6->extension();
            $nameFile = "{$name}.{$extension}";
            $upload = $request->content_link6->storeAs('public/uploads/pages/', $nameFile, 'azure');

            $att = DB::table('pages')->where('pagename', ($page))->update(['title_link6' => ($nameFile)]);
            //echo $nameFile; exit;
        }
        //echo 'content_link6'; exit;

        //Textos de Banner
        DB::table('banners')->where(['pagename' => ($page)])->update([
            'desktop_title' => ($banner_desktop_title),
            'mobile_title' => ($banner_mobile_title),
            'updated_at' => \Carbon\Carbon::now(),
        ]);

        //Textos de Conteúdo
        DB::table('pages')->where(['pagename' => ($page)])->update([
            'title' => ($content_title),
            'title_link' => ($content_link),
            'text' => ($content_text),
            'title2' => ($content_title2),
            'text2' => ($content_text2),
            'title3' => ($content_title3),
            'title_link3' => ($content_link3),
            'text3' => ($content_text3),
            'title4' => ($content_title4),
            'title_link4' => ($content_link4),
            'text4' => ($content_text4),
            'title5' => ($content_title5),
            'title_link5' => ($content_link5),
            'text5' => ($content_text5),
            'title6' => ($content_title6),
            'subtitle6' => ($subtitle6),
            'text6' => ($content_text6),
            'midia_kit' => ($midia_kit),
            'midia_kit_completo' => ($midia_kit_completo),
            'updated_at' => \Carbon\Carbon::now(),
        ]);

        //Textos de Icones
        DB::table('announce')->where(['id' => '1'])->update(['title' => ($icon_name)]);
        DB::table('announce')->where(['id' => '2'])->update(['title' => ($icon_name2)]);
        DB::table('announce')->where(['id' => '3'])->update(['title' => ($icon_name3)]);
        DB::table('announce')->where(['id' => '4'])->update(['title' => ($icon_name4)]);
        DB::table('announce')->where(['id' => '5'])->update(['title' => ($icon_name5)]);
        DB::table('announce')->where(['id' => '6'])->update(['title' => ($icon_name6)]);

        return redirect('admin/home')->with('alert', 'Home Atualizada!');

    }

    public function deletepreco(Request $request)
    {
        $att = DB::table('pages')->where('pagename', 'home')->update(['title_link6' => null]);

        return redirect('admin/home')->with('alert', 'Tabela de Preços Deletada!');
    }
}
