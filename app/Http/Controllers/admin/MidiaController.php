<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use \DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class MidiaController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
                $banner = DB::table('banners')->select('banners.*')->where('pagename','midia')->first();
        $page = DB::table('pages')->select('pages.*')->where('pagename','midia')->first();
        
        return view('admin.midia', compact('banner','page'));
    }
    
        public function update(Request $request){
        $page   = 'midia'; 
            
                    $banner_desktop_title      =$request->get('banner_desk_title');
        $banner_mobile_title    =$request->get('banner_mobile_title');

        $content_title          =$request->get('content_title');
        $content_text           =$request->get('content_text');
        $content_title2         =$request->get('content_title2');
        $content_text2          =$request->get('content_text2');
        
       
        //Banners


            
       if($request->hasFile('banner_mobile') && $request->file('banner_mobile')->isValid()) {
        $name = uniqid(date('HisYmd'));
        $extension = $request->banner_mobile->extension();
        $nameFile2 = "{$name}.{$extension}";
        $upload = $request->banner_mobile->storeAs('public/uploads/banners/', $nameFile2, 'azure');

$att =     DB::table('banners')->where('pagename',($page))->update(['mobile' => ($nameFile2)]);
}   
                   if($request->hasFile('banner_desktop') && $request->file('banner_desktop')->isValid()) {
        $name = uniqid(date('HisYmd'));
        $extension = $request->banner_desktop->extension();
        $nameFile = "{$name}.{$extension}";
        $upload = $request->banner_desktop->storeAs('public/uploads/banners/', $nameFile,'azure');

$att =     DB::table('banners')->where('pagename',($page))->update(['desktop' => ($nameFile)]);
}   
            
        //Midias

       if($request->hasFile('midia1') && $request->file('midia1')->isValid()) {
        $name = uniqid(date('HisYmd'));
        $extension = $request->midia1->extension();
        $nameFile = "{$name}.{$extension}";
        $upload = $request->midia1->storeAs('public/uploads/pages/', $nameFile, 'azure');

$att =     DB::table('pages')->where('pagename',($page))->update(['image1' => ($nameFile)]);
}   
        if($request->hasFile('midia2') && $request->file('midia2')->isValid()) {
        $name = uniqid(date('HisYmd'));
        $extension = $request->midia2->extension();
        $nameFile = "{$name}.{$extension}";
        $upload = $request->midia2->storeAs('public/uploads/pages/', $nameFile, 'azure');

$att =     DB::table('pages')->where('pagename',($page))->update(['image2' => ($nameFile)]);
}  
        
            
        //Textos de Banner
            DB::table('banners')->where(['pagename'=>($page)])->update([
            'desktop_title' => ($banner_desktop_title), 
            'mobile_title'=>($banner_mobile_title),
            'updated_at' => \Carbon\Carbon::now()
        ]);
            
        //Textos de Conteúdo
            DB::table('pages')->where(['pagename'=>($page)])->update([
            'title' => ($content_title),
            'text' => ($content_text),
            'title2' => ($content_title2),
            'text2' => ($content_text2),

            'updated_at' => \Carbon\Carbon::now()
        ]);

        return redirect('admin/midia')->with('alert', 'Midia Atualizada!');

    }
}
