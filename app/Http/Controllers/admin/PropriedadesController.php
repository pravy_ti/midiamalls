<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Validator;
use \DB;

class PropriedadesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $banner = DB::table('banners')->select('banners.*')->where('pagename', 'propriedades')->first();
        $page = DB::table('pages')->select('pages.*')->where('pagename', 'propriedades')->first();
        $propriedades = DB::table('properties')->select('properties.*')->where('id_shopping', '0')->get();

        return view('admin.propriedades', compact('banner', 'page', 'propriedades'));
    }

    public function update(Request $request)
    {
        $page = 'propriedades';

        $banner_desktop_title = $request->get('banner_desk_title');
        $banner_mobile_title = $request->get('banner_mobile_title');

        //Banners

        if ($request->hasFile('banner_mobile') && $request->file('banner_mobile')->isValid()) {
            $name = uniqid(date('HisYmd'));
            $extension = $request->banner_mobile->extension();
            $nameFile2 = "{$name}.{$extension}";
            $upload = $request->banner_mobile->storeAs('public/uploads/banners/', $nameFile2, 'azure');

            $att = DB::table('banners')->where('pagename', ($page))->update(['mobile' => ($nameFile2)]);
        }
        if ($request->hasFile('banner_desktop') && $request->file('banner_desktop')->isValid()) {
            $name = uniqid(date('HisYmd'));
            $extension = $request->banner_desktop->extension();
            $nameFile = "{$name}.{$extension}";
            $upload = $request->banner_desktop->storeAs('public/uploads/banners/', $nameFile, 'azure');

            $att = DB::table('banners')->where('pagename', ($page))->update(['desktop' => ($nameFile)]);
        }

        //Textos de Banner
        DB::table('banners')->where(['pagename' => ($page)])->update([
            'desktop_title' => ($banner_desktop_title),
            'mobile_title' => ($banner_mobile_title),
            'updated_at' => \Carbon\Carbon::now(),
        ]);

        return redirect('admin/propriedades')->with('alert', 'Propriedades Atualizado!');

    }

    public function novo()
    {
        $page = 'propriedades';

        $shoppings = DB::table('shoppings')->select('shoppings.*')->orderby('name', 'asc')->get();

        return view('admin.propriedades-new', compact('shoppings'));
    }

    public function inserirnovo(Request $request)
    {
        $shoppings = DB::table('shoppings')->select('shoppings.*')->orderby('name', 'asc')->get();

        $dados = $request->all();
        $rules = array(
            'icon' => 'required|image|max:2048', //10mb  10240
            'iconh' => 'required|image|max:2048', // 2mb
            'bannerprop' => 'required|image|max:2048',
            'propriedade' => 'required'
        );

        $messages = array(
            'icon.required' => 'O campo icone é obrigatório',
            'icon.image' => 'O campo icone deve ser uma imagem.',
            'icon.max'  => 'O campo icone deve ter no máximo 2Mb.',
            'iconh.required' => 'O campo icone hover é obrigatório',
            'iconh.image' => 'O campo icone hover deve ser uma imagem.',
            'iconh.max'  => 'O campo icone hover deve ter no máximo 2Mb.',
            'bannerprop.required' => 'O campo banner é obrigatório',
            'bannerprop.image' => 'O campo banner deve ser uma imagem.',
            'bannerprop.max'  => 'O campo banner deve ter no máximo 2Mb.',
            'propriedade.required' => 'O campo nome do formato é obrigatório',
        );

        $validator = Validator::make($dados, $rules, $messages);

        if ($validator->fails()) {
            return redirect('admin/propriedades')->withErrors($validator)->withInput();
        }
        
        
        
        $propriedade = $request->get('propriedade');

        if ($request->hasFile('icon') && $request->file('icon')->isValid()) {
            $name = $request->icon->getClientOriginalName();
            $extension = $request->icon->extension();
            $nameFile = "{$name}";
            $upload = $request->icon->storeAs('public/uploads/icons/', $nameFile, 'azure');

        }
        if ($request->hasFile('iconh') && $request->file('iconh')->isValid()) {
            $name = $request->iconh->getClientOriginalName();
            $extension = $request->iconh->extension();
            $nameFile2 = "{$name}";
            $upload = $request->iconh->storeAs('public/uploads/icons/', $nameFile2, 'azure');

        }
        if ($request->hasFile('bannerprop') && $request->file('bannerprop')->isValid()) {
            $name = $request->bannerprop->getClientOriginalName();
            $extension = $request->iconh->extension();
            $nameFile3 = "{$name}";
            $upload = $request->bannerprop->storeAs('public/uploads/propriedades/', $nameFile3, 'azure');

        }

        $att = DB::table('properties')->insert([
            'id_shopping' => '0',
            'name' => ($propriedade),
            'icon' => ($nameFile),
            'icon_hover' => ($nameFile2),
            'image' => ($nameFile3),
        ]);

        $shopping_prop = $request->get('shop_prop');
        $images = $request->file('shop_image');

        if (is_array($images)) {
            foreach (array_combine($shopping_prop, $images) as $shop => $image) {

                $name = $image->getClientOriginalName();
                $image->storeAs('public/uploads/banners/', $name, 'azure');

                $att2 = DB::table('properties')->insert([
                    'id_shopping' => ($shop),
                    'icon' => null,
                    'icon_hover' => null,
                    'image' => ($name),
                    'name' => ($propriedade),
                ]);
            }
        } else {
            foreach ($shopping_prop as $shop) {

                // $name=$image->getClientOriginalName();
                //$image->storeAs('public/uploads/banners/', $name, 'azure');

                $att2 = DB::table('properties')->insert([
                    'id_shopping' => ($shop),
                    'icon' => null,
                    'icon_hover' => null,
                    'image' => null,
                    'name' => ($propriedade),
                ]);
            }
        }

        return redirect('admin/propriedades');
    }

    public function editarpropriedade($slug = null)
    {

        $propriedades = DB::table('properties')->select('properties.*')->where('id_shopping', '0')->get();

        foreach ($propriedades as $prop) {

            if ($slug == $prop->id) {

                $shoppings = DB::table('shoppings')->select('shoppings.*')->get();

                $shopro = DB::table('properties')
                    ->join('shoppings', 'properties.id_shopping', '=', 'shoppings.id')
                    ->select('properties.*', 'properties.id as idprop', 'shoppings.id', 'shoppings.name as shoname')->get();

                $props = [

                    'id' => $prop->id,
                    'icon' => $prop->icon,
                    'iconh' => $prop->icon_hover,
                    'imagem' => $prop->image,
                    'id_shop' => $prop->id_shopping,
                    'name' => $prop->name,

                ];}

        }

        // dd($shopro);
        return view('admin.propriedades-edit', ['props' => $props], compact('shoppings', 'shopro'));

    }

    public function updateprop(Request $request)
    {
        $shoppings = DB::table('shoppings')->select('shoppings.*')->orderby('name', 'asc')->get();

        $propriedade = $request->get('propriedade');
        $idprop = $request->get('idprop');

        $dados = $request->all();
        $rules = array(
            'icon' => 'image|max:2048', //10mb  10240
            'iconh' => 'image|max:2048', // 2mb
            'bannerprop' => 'image|max:2048',
            'propriedade' => 'required'
        );

        $messages = array(
            'icon.image' => 'O campo icone deve ser uma imagem.',
            'icon.max'  => 'O campo icone deve ter no máximo 2Mb.',
            'iconh.image' => 'O campo icone hover deve ser uma imagem.',
            'iconh.max'  => 'O campo icone hover deve ter no máximo 2Mb.',
            'bannerprop.image' => 'O campo banner deve ser uma imagem.',
            'bannerprop.max'  => 'O campo banner deve ter no máximo 2Mb.',
            'propriedade.required' => 'O campo nome do formato é obrigatório',
        );

        $validator = Validator::make($dados, $rules, $messages);

        if ($validator->fails()) {
            return redirect('admin/propriedades/' . $idprop)->withErrors($validator)->withInput();
        }

        if ($request->hasFile('icon') && $request->file('icon')->isValid()) {
            $name = $request->icon->getClientOriginalName();
            $extension = $request->icon->extension();
            $nameFile = "{$name}";
            $upload = $request->icon->storeAs('public/uploads/icons/', $nameFile, 'azure');

            $att = DB::table('properties')->where('id', ($idprop))->update([
                'icon' => ($nameFile),
            ]);

        }
        if ($request->hasFile('iconh') && $request->file('iconh')->isValid()) {
            $name = $request->iconh->getClientOriginalName();
            $extension = $request->iconh->extension();
            $nameFile2 = "{$name}";
            $upload = $request->iconh->storeAs('public/uploads/icons/', $nameFile2, 'azure');

            $att = DB::table('properties')->where('id', ($idprop))->update([
                'icon_hover' => ($nameFile2),
            ]);

        }
        if ($request->hasFile('bannerprop') && $request->file('bannerprop')->isValid()) {
            $name = $request->bannerprop->getClientOriginalName();
            $extension = $request->bannerprop->extension();
            $nameFile3 = "{$name}";
            $upload = $request->bannerprop->storeAs('public/uploads/propriedades/', $nameFile3, 'azure');

            $att = DB::table('properties')->where('id', ($idprop))->update([
                'image' => ($nameFile3),
            ]);

        }

        $att = DB::table('properties')->where('id', ($idprop))->update([
            'name' => ($propriedade),
        ]);

        $idshopup = $request->get('shopup');
        $shopping_prop = $request->get('shop_prop');

        if (isset($idshopup)) {
            foreach ($idshopup as $shopup) {
                $att = DB::table('properties')->where('id', ($shopup))->delete();
            }}

        if (isset($shopping_prop)) {
            foreach ($shopping_prop as $shop) {
                $att = DB::table('properties')->insert(['name' => ($propriedade), 'id_shopping' => ($shop)]);
            }}
//
        //        foreach ($shopping_prop as $shop) {
        //
        //        if(isset($shop)){
        //            $teste = DB::table('properties')->where('id',($idshopup))->get();
        //
        //
        //            if($teste == null){
        //            $att =   DB::table('properties2')->insert(['name' => ($propriedade),'id_shopping' => ($shop)]);
        //            }else{
        //
        //            }
        //
        //
        //
        //
        //        }else{
        //             $teste = DB::table('properties')->where('id',($idshopup))->get();
        //             if(!$teste){
        //            }else{
        //          // $att =   DB::table('properties')->where('id',($idshopup))->delete();
        //            }
        //        };
        //
        //
        //        /*$att2= DB::table('properties')->where('id',($idshopup))->update([
        //                        'image' => null,
        //                        'name'        => ($propriedade)
        //                    ]);*/
        //
        //        }
        //
        //

        return redirect('admin/propriedades');
    }

    public function delete(Request $request)
    {

        $idprop = $request->get('idshop');

        $att = DB::table('properties')->where('id', ($idprop))->delete();

        return redirect('admin/propriedades');
    }

}
