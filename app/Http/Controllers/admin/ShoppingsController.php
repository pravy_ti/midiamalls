<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use \DB;
use Validator;

class ShoppingsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        $banner = DB::table('banners')->select('banners.*')->where('pagename', 'shoppings')->first();
        $page = DB::table('pages')->select('pages.*')->where('pagename', 'shoppings')->first();
        $shoppings = DB::table('shoppings')->select('shoppings.*')->get();

        return view('admin.shoppings', compact('banner', 'page', 'shoppings'));
    }

    public function novoshopping()
    {
        $id_shop = DB::table('shoppings')->select('shoppings.id')->orderby('id', 'desc')->first();
        $propriedades = DB::table('properties')->select('properties.*')->where('id_shopping', '0')->get();

        return view('admin.shoppings-new', compact('propriedades', 'id_shop'));
    }

    public function update(Request $request)
    {
        $page = 'shoppings';

        $dados = $request->all();
        $rules = array(
            'banner_desktop' => 'image|max:2048', //10mb  10240
            'banner_mobile' => 'image|max:2048', // 2mb
        );

        $messages = array(
            'banner_desktop.image' => 'O campo Banner desktop deve ser uma imagem.',
            'banner_desktop.max'  => 'O campo Banner desktop deve ter no máximo 2Mb.',
            'banner_mobile.image' => 'O campo Banner mobile deve ser uma imagem.',
            'banner_mobile.max'  => 'O campo Banner mobile deve ter no máximo 2Mb.'
        );

        $validator = Validator::make($dados, $rules, $messages);

        if ($validator->fails()) {
            return redirect('admin/nossos-shoppings')->withErrors($validator)->withInput();
        }


        $banner_desktop_title = $request->get('banner_desk_title');
        $banner_mobile_title = $request->get('banner_mobile_title');
        $content_title = $request->get('content_title');
        $content_text = $request->get('content_text');
        $content_title2 = $request->get('content_title2');
        $midia_kit = $request->get('midia_kit');

        $tabela = $request->get('tabela');
        $midia = $request->get('midiaglobal');

        //Banners

        if ($request->hasFile('banner_mobile') && $request->file('banner_mobile')->isValid()) {
            $name = uniqid(date('HisYmd'));
            $extension = $request->banner_mobile->extension();
            $nameFile2 = "{$name}.{$extension}";
            $upload = $request->banner_mobile->storeAs('public/uploads/banners/', $nameFile2, 'azure');

            $att = DB::table('banners')->where('pagename', ($page))->update(['mobile' => ($nameFile2)]);
        }
        if ($request->hasFile('banner_desktop') && $request->file('banner_desktop')->isValid()) {
            $name = uniqid(date('HisYmd'));
            $extension = $request->banner_desktop->extension();
            $nameFile = "{$name}.{$extension}";
            $upload = $request->banner_desktop->storeAs('public/uploads/banners/', $nameFile, 'azure');

            $att = DB::table('banners')->where('pagename', ($page))->update(['desktop' => ($nameFile)]);
        }

        //Midias

        if ($request->hasFile('midiakit') && $request->file('midiakit')->isValid()) {
            $name = $request->midiakit->getClientOriginalName();
            $extension = $request->midiakit->extension();
            $nameFile = "{$name}";
            $upload = $request->midiakit->storeAs('public/uploads/files/', $nameFile, 'azure');

            $att = DB::table('pages')->where('pagename', ($page))->update(['image1' => ($nameFile)]);
        }
        if ($request->hasFile('precos') && $request->file('precos')->isValid()) {
            $name = $request->precos->getClientOriginalName();
            $extension = $request->precos->extension();
            $nameFile = "{$name}";
            $upload = $request->precos->storeAs('public/uploads/files/', $nameFile, 'azure');

            $att = DB::table('pages')->where('pagename', ($page))->update(['image2' => ($nameFile)]);
        }

        //Textos de Banner
        DB::table('banners')->where(['pagename' => ($page)])->update([
            'desktop_title' => ($banner_desktop_title),
            'mobile_title' => ($banner_mobile_title),
            'updated_at' => \Carbon\Carbon::now(),
        ]);

        //Textos de Conteúdo
        DB::table('pages')->where(['pagename' => ($page)])->update([
            'title' => ($content_title),
            'text' => ($content_text),
            'title2' => ($content_title2),
            'midia_kit' => $midia_kit,
            'updated_at' => \Carbon\Carbon::now(),
        ]);

        if ($tabela == 'on') {$att = DB::table('pages')->where(['pagename' => ($page)])->update(['image2' => null]);};
        if ($midia == 'on') {$att = DB::table('pages')->where(['pagename' => ($page)])->update(['image1' => null]);};

        return redirect('admin/nossos-shoppings')->with('alert', 'Shoppings Atualizado!');

    }

    public function inserirnovo(Request $request)
    {
        
        $shopping = $request->get('shopping');
        $cidade = $request->get('cidade');
        $uf = $request->get('uf');
        $visitas = $request->get('visitas');
        $abl = $request->get('abl');
        $classea = $request->get('a');
        $classeb1 = $request->get('b1');
        $classeb2 = $request->get('b2');
        $classecd = $request->get('cd');
        $destaque = $request->get('destaque');
        $class = $request->get('class');
        $site = $request->get('site');

        if ($request->hasFile('midiakit') && $request->file('midiakit')->isValid()) {
            $name = $request->midiakit->getClientOriginalName();
            $extension = $request->midiakit->extension();
            $nameFile = "{$name}";
            $upload = $request->midiakit->storeAs('public/uploads/files/', $nameFile, 'azure');

        } else { $nameFile = null;}
        if ($request->hasFile('thumbnail') && $request->file('thumbnail')->isValid()) {
            $name = uniqid(date('HisYmd'));
            $extension = $request->thumbnail->extension();
            $nameFile2 = "{$name}.{$extension}";
            $upload = $request->thumbnail->storeAs('public/uploads/shoppings/', $nameFile2, 'azure');
        }
        $att = DB::table('shoppings')->insert([
            'name' => ($shopping),
            'image' => ($nameFile2),
            'city' => ($cidade),
            'uf' => ($uf),
            'visits' => ($visitas),
            'abl' => ($abl),
            'class_a' => ($classea),
            'class_b1' => ($classeb1),
            'class_b2' => ($classeb2),
            'class_cd' => ($classecd),
            'midiakit' => ($nameFile),
            'destaque' => ($destaque),
            'class' => ($class),
            'site' => ($site),
        ]);

        $prop = $request->get('prop');

        $id_shop = DB::table('shoppings')->select('shoppings.id')->orderby('id', 'desc')->first();
        if (isset($prop)) {
            foreach ($prop as $pro) {
                $teste = DB::table('properties')->select('properties.name')->where('id', ($pro))->first();

                $att2 = DB::table('properties')->insert([
                    'id_shopping' => ($id_shop->id),
                    'icon' => null,
                    'icon_hover' => null,
                    'image' => null,
                    'name' => ($teste->name),
                ]);

            }
        }

        return redirect('admin/nossos-shoppings')->with('alert', 'Shoppings Inserido!');

    }

    public function editshopping(Request $request)
    {

        $shopping = $request->get('shopping');
        $cidade = $request->get('cidade');
        $uf = $request->get('uf');
        $visitas = $request->get('visitas');
        $abl = $request->get('abl');
        $classea = $request->get('a');
        $classeb1 = $request->get('b1');
        $classeb2 = $request->get('b2');
        $classecd = $request->get('cd');
        $destaque = $request->get('destaque');
        $idshop = $request->get('idshop');
        $class = $request->get('class');
        $site = $request->get('site');

        if ($request->hasFile('midiakit') && $request->file('midiakit')->isValid()) {
            $name = $request->midiakit->getClientOriginalName();
            $extension = $request->midiakit->extension();
            $nameFile = "{$name}";
            $upload = $request->midiakit->storeAs('public/uploads/files/', $nameFile, 'azure');

            $att = DB::table('shoppings')->where('id', ($idshop))->update(['midiakit' => ($nameFile)]);

        }
        if ($request->hasFile('thumbnail') && $request->file('thumbnail')->isValid()) {
            $name = uniqid(date('HisYmd'));
            $extension = $request->thumbnail->extension();
            $nameFile2 = "{$name}.{$extension}";
            $upload = $request->thumbnail->storeAs('public/uploads/shoppings/', $nameFile2, 'azure');

            $att = DB::table('shoppings')->where('id', ($idshop))->update(['image' => ($nameFile2)]);
        }

        $att = DB::table('shoppings')->where('id', ($idshop))->update([
            'name' => ($shopping),
            'city' => ($cidade),
            'uf' => ($uf),
            'visits' => ($visitas),
            'abl' => ($abl),
            'class_a' => ($classea),
            'class_b1' => ($classeb1),
            'class_b2' => ($classeb2),
            'class_cd' => ($classecd),
            'destaque' => ($destaque),
            'class' => ($class),
            'site' => ($site),
        ]);

        return redirect('admin/nossos-shoppings')->with('alert', 'Shopping Atualizado!');

    }

    public function deleteshopping(Request $request)
    {

        $idshop = $request->get('idshop');

        $att = DB::table('shoppings')->where('id', ($idshop))->delete();

        return redirect('admin/nossos-shoppings')->with('alert', 'Shopping Deletado!');

    }

    public function deletemidia(Request $request)
    {

        $idshop = $request->get('idshop');

        $att = DB::table('shoppings')->where('id', ($idshop))->update(['midiakit' => null]);

        return redirect('admin/nossos-shoppings')->with('alert', 'Shopping Deletado!');

    }public function deletemidiageral(Request $request)
    {
        $att = DB::table('pages')->where('pagename', 'shoppings')->update(['image1' => null]);

        return redirect('admin/nossos-shoppings')->with('alert', 'MidiaKit Geral Deletada!');
    }

    public function deletepreco(Request $request)
    {
        $att = DB::table('pages')->where('pagename', 'shoppings')->update(['image2' => null]);

        return redirect('admin/nossos-shoppings')->with('alert', 'Tabela de Preços Deletada!');
    }

}
