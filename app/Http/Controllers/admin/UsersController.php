<?php

namespace App\Http\Controllers\admin;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use URL;
use Session;
use Request;
use App\Models\Administradores as User;
use App\Models\Roles;
//use Illuminate\Support\Facades\Input;
use Validator;
use Redirect;
use Mail;
use Hash;

class UsersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $section = 'users';

            $users = User::all();

        
        return view('admin.users.list',compact('users','section'));
    }
    
    
   	public function create()
    {
        $section    = 'users';
         


        return view('admin.users.create', [
            'section'       => $section, 

        ]);
    }

    public function edit($id = null)
    {
        $user = User::find($id);

        return view('admin.users.create', [
            'section'       => 'users',
            'user'          => $user,
        ]);
    }

    public function destroy($id)
    {
        $entity = User::findOrFail($id);
        $entity->delete();
        return Redirect::route('admin.users')->with('sucess', 'Registro apagado com sucesso!');
    }

    public function changePassword()
    {


        $user = Auth::user();

        $section = 'chagePassword';
        return view('admin.users.password', [
            'section'   => $section,
            'user'      => $user,
        ]);
    }

    public function storePassword($id)
    {
        $dados = $request->all();
        Validator::extend('old_password', function ($attribute, $value, $parameters, $validator) {
            return Hash::check($value, current($parameters));
        });
        
        $user = User::find($id);
        $rules = array(
            'old_password'              =>  'required|old_password:' . Auth::user()->password,
            'password'                  =>  'required|min:6|confirmed',
            'password_confirmation'     =>  'required|min:6'
        );
        $validator = Validator::make($dados, $rules);

        if(!$validator->fails()){

            $senha              = bcrypt($dados['password']);
            $user->password     = $senha;

            $user->save();
         
            return Redirect::route('admin.users.changePassword')->with('sucess', 'Senha alterada com sucesso!');
        }else{
            return Redirect::route('admin.users.changePassword')
                ->withErrors($validator)
                ->withInput();
            
            
        }
    }


    public function store(Request $request, $id = null){
        
        $dados = Request::all();

        if($id){
            $user = User::find($id);
            $rules = array(
                'nome'      =>'required',  
                'email'     =>'required|min:5|email|unique:users,email,'.$id,
                'senha'     =>'required|min:6',

            );
        }else{
            $user = new User;
            $rules = array(
                'nome'      => 'required',
                'email'     => 'required|min:5|email|unique:users,email',
                'senha'     => 'required|min:6',
             ); 
        }
        $messages = [
            'email.min' => 'O e-mail deve conter no mínimo 5 caracteres',
            'email.email' => 'O e-mail deve ser um e-mail válido',
            'required' =>'O campo :attribute é obrigatório',
            'senha.min'  => 'Senha deve ter no mínimo 6 caracteres',
            'unique' => 'Este :attribute já foi cadastrado.',
        ];
        $validator = Validator::make($dados, $rules, $messages);

        if(!$validator->fails()){

            $user->name             = $dados['nome'];
            $user->email            = $dados['email'];
            $user->role             = $dados['role'];

            if(isset($dados['senha'])){
                $senha = bcrypt($dados['senha']);
                $user->password       = $senha;
            }else{
                return Redirect::route('admin.users.create')
                ->withErrors($validator)
                ->withInput();;
            }


            $user->save();
         
            return Redirect::route('admin.users')->with('sucess', 'Registro efetuado com sucesso!');
        }else{
            if($id){
                return Redirect::route('admin.users')
                ->withErrors($validator)
                ->withInput();
            }else{
                return Redirect::route('admin.users.create')
                ->withErrors($validator)
                ->withInput();
            }
            
            
        }
    }
}