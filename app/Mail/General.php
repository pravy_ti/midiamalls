<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;


class General extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($text, $title)
    {
        $this->text = $text;
        $this->title = $title;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
       /* return $this->view('emails.general')
                    ->with([
                        'text' => $this->text,
                        'title' => $this->title
                    ])
                    ->from('noreply@brmallsnoreply.com.br')
                    ->subject($this->title); */
    }

    
}
