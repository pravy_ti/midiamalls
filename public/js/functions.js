//Scroll Home.

$('.fixed-menu li a').click(function(e){
      e.preventDefault();
           $("html, body").animate({ scrollTop: $($(this).attr('href')).offset().top - 0 }, 500);
});

$('#anchorshop').click(function(e){
      e.preventDefault();
           $("html, body").animate({ scrollTop: $($(this).attr('href')).offset().top - 0 }, 500);
});



$('#contact_footer a').click(function(e){
e.preventDefault();
           $("html, body").animate({ scrollTop: $($(this).attr('href')).offset().top - 50 }, 500);
            document.querySelector('.nav-menu').classList.remove('open');
        document.querySelector('#header').classList.remove('open');
        document.querySelector('#btn-menu').classList.remove('open');
});

// Linha do Scroll Home.
       function scrollLine(){
           if( $('#contact').offset().top - $(window).scrollTop() <= $(window).height() * 0.05 ){
               $('.fixed-menu ul').addClass('white');
               $('.fixed-menu ul .line').height($('.fixed-menu ul li:nth-child(7)').height());
               $('.fixed-menu ul .line').css('top', $('.fixed-menu ul li:nth-child(7)').position().top + 'px');
           }else if( $('#midiakit').offset().top - $(window).scrollTop() <= $(window).height() * 0.05 ){
               $('.fixed-menu ul').removeClass('white');
               $('.fixed-menu ul .line').height($('.fixed-menu ul li:nth-child(6)').height());
               $('.fixed-menu ul .line').css('top', $('.fixed-menu ul li:nth-child(6)').position().top + 'px');
           }else if( $('#propriedades').offset().top - $(window).scrollTop() <= $(window).height() * 0.05 ){
               $('.fixed-menu ul').removeClass('white');
               $('.fixed-menu ul .line').height($('.fixed-menu ul li:nth-child(5)').height());
               $('.fixed-menu ul .line').css('top', $('.fixed-menu ul li:nth-child(5)').position().top + 'px');
           }else if( $('#malls').offset().top - $(window).scrollTop() <= $(window).height() * 0.1 ){
               $('.fixed-menu ul').removeClass('white');
               $('.fixed-menu ul .line').height($('.fixed-menu ul li:nth-child(4)').height());
               $('.fixed-menu ul .line').css('top', $('.fixed-menu ul li:nth-child(4)').position().top + 'px');
           }else if( $('#anunciar').offset().top - $(window).scrollTop() <= $(window).height() * 0.1 ){
               $('.fixed-menu ul').addClass('white');
               $('.fixed-menu ul .line').height($('.fixed-menu ul li:nth-child(3)').height());
               $('.fixed-menu ul .line').css('top', $('.fixed-menu ul li:nth-child(3)').position().top + 'px');
           }else if( $('#midia').offset().top - $(window).scrollTop() <= $(window).height() * 0.1 ){
               $('.fixed-menu ul').removeClass('white');
               $('.fixed-menu ul .line').height($('.fixed-menu ul li:nth-child(2)').height());
               $('.fixed-menu ul .line').css('top', $('.fixed-menu ul li:nth-child(2)').position().top + 'px');
           }else{
               $('.fixed-menu ul').removeClass('white');
               $('.fixed-menu ul .line').height($('.fixed-menu ul li:nth-child(1)').height());
               $('.fixed-menu ul .line').css('top', $('.fixed-menu ul li:nth-child(1)').position().top + 'px');
           }

           if( $('.fixed-menu ul').hasClass('stopped') && $('.menu-free').offset().top < $('.stop-fixed').offset().top  ) {
               setTimeout(function(){
                   $('.fixed-menu ul').removeClass('stopped');
                   $('.fixed-menu ul').css('position', '');
                   $('.fixed-menu ul').css('top', '');
               }, 500);
           }else if( $('.menu-stopper').offset().top >= $('.stop-fixed').offset().top ){
               $('.fixed-menu ul').css('position', 'absolute').addClass('stopped');
               $('.fixed-menu ul').css('top', $('.fixed-menu ul').height() / 2 + $('.stop-fixed').offset().top - $('.fixed-menu ul').height() + 1 + 'px');
           }
       }


        $(window).scroll(function()
        {
            scrollLine();
        }).resize(function()
        {
            $('#malls .cards-holder, #propriedades .cards-holder, #variations .cards-holder, #work .mini-cards-holder, #our-people .mini-cards-holder').css('min-width', 'calc(100% + 15px + ' + $('#malls .container').offset().left + 'px)');
            $('#malls .cards-holder, #propriedades .cards-holder, #variations .cards-holder, #work .mini-cards-holder, #our-people .mini-cards-holder').css('padding-right', $('#malls .container').offset().left + 'px');
            $('#malls .cards-holder .slick-arrow, #variations .cards-holder .slick-arrow, #work .mini-cards-holder .slick-arrow, #our-people .mini-cards-holder .slick-arrow').css('margin-right', $('#malls .container').offset().left + 'px');
        });

// Configuração para menu

$('.holder-hamburger').click(function(e){

    if (!document.querySelector('.nav-menu').classList.contains('open')) {
        document.querySelector('.nav-menu').classList.add('open');
        document.querySelector('#header').classList.add('open');
        document.querySelector('#btn-menu').classList.add('open');
    } else {
        document.querySelector('.nav-menu').classList.remove('open');
        document.querySelector('#header').classList.remove('open');
        document.querySelector('#btn-menu').classList.remove('open');
    }


                       });




$('.prop1').click(function(){

        $('.prop1 p').removeClass('-nblue');
        $('.prop1 p').addClass('-orange');
        $('.prop2 p').removeClass('-orange');
        $('.prop2 p').addClass('-nblue');
        $('.prop3 p').removeClass('-orange');
        $('.prop3 p').addClass('-nblue');
        $('.prop4 p').removeClass('-orange');
        $('.prop4 p').addClass('-nblue');
        $('.prop5 p').removeClass('-orange');
        $('.prop5 p').addClass('-nblue');
                       });

$('.prop2').click(function(){
        $('.prop1 p').removeClass('-orange');
        $('.prop1 p').addClass('-nblue');
        $('.prop2 p').removeClass('-nblue');
        $('.prop2 p').addClass('-orange');
        $('.prop3 p').removeClass('-orange');
        $('.prop3 p').addClass('-nblue');
        $('.prop4 p').removeClass('-orange');
        $('.prop4 p').addClass('-nblue');
        $('.prop5 p').removeClass('-orange');
        $('.prop5 p').addClass('-nblue');
                       });

$('.prop3').click(function(){
        $('.prop1 p').removeClass('-orange');
        $('.prop1 p').addClass('-nblue');
        $('.prop2 p').removeClass('-orange');
        $('.prop2 p').addClass('-nblue');
        $('.prop3 p').removeClass('-nblue');
        $('.prop3 p').addClass('-orange');
        $('.prop4 p').removeClass('-orange');
        $('.prop4 p').addClass('-nblue');
        $('.prop5 p').removeClass('-orange');
        $('.prop5 p').addClass('-nblue');
                       });

$('.prop4').click(function(){
        $('.prop1 p').removeClass('-orange');
        $('.prop1 p').addClass('-nblue');
        $('.prop2 p').removeClass('-orange');
        $('.prop2 p').addClass('-nblue');
        $('.prop3 p').removeClass('-orange');
        $('.prop3 p').addClass('-nblue');
        $('.prop4 p').removeClass('-nblue');
        $('.prop4 p').addClass('-orange');
        $('.prop5 p').removeClass('-orange');
        $('.prop5 p').addClass('-nblue');
                       });

$('.prop5').click(function(){
        $('.prop1 p').removeClass('-orange');
        $('.prop1 p').addClass('-nblue');
        $('.prop2 p').removeClass('-orange');
        $('.prop2 p').addClass('-nblue');
        $('.prop3 p').removeClass('-orange');
        $('.prop3 p').addClass('-nblue');
        $('.prop4 p').removeClass('-orange');
        $('.prop4 p').addClass('-nblue');
        $('.prop5 p').removeClass('-nblue');
        $('.prop5 p').addClass('-orange');

                       });

//Teste





       $('#malls .cards-holder, #propriedades .cards-holder, #variations .cards-holder, #work .mini-cards-holder, #our-people .mini-cards-holder').css('min-width', 'calc(100% + 15px + ' + $('#malls .container,#malls_interna .cards-holder, #propriedades .container').offset().left + 'px)');
       $('#malls .cards-holder, #propriedades .cards-holder, #variations .cards-holder, #work .mini-cards-holder, #our-people .mini-cards-holder').css('padding-right', $('#malls .container, #propriedades .container').offset().left + 'px');


        $('#malls .cards-holder').slick({
            infinite: false,
            slidesToShow: 3,
            slidesToScroll: 1,
            dots: false,
            arrows: true,
            speed: 750,
            prevArrow: '<button type="button" class="slick-prev"><i class="icon-next"></i></button>',
            nextArrow: '<button type="button" class="slick-next"><i class="icon-next"></i></button>',
            responsive: [
                {
                    breakpoint: 991,
                    settings: {
                        slidesToShow: 2,
                        dots: true,
                        arrows: false
                    }
                },
                {
                    breakpoint: 576,
                    settings: {
                        slidesToShow: 1,
                        dots: true,
                        arrows: false
                    }
                },
            ]
        });

        $('#our-people .mini-cards-holder').slick({
            infinite: false,
            slidesToShow: 4,
            slidesToScroll: 1,
            dots: false,
            arrows: true,
            speed: 750,
            prevArrow: '<button type="button" class="slick-prev"><i class="icon-next"></i></button>',
            nextArrow: '<button type="button" class="slick-next"><i class="icon-next"></i></button>',
            responsive: [
                {
                    breakpoint: 1440,
                    settings: {
                        slidesToShow: 3,
                        dots: false,
                        arrows: true
                    }
                },
                {
                    breakpoint: 1280,
                    settings: {
                        slidesToShow: 2,
                        dots: false,
                        arrows: true
                    }
                },
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 2,
                        dots: true,
                        arrows: false
                    }
                },
                {
                    breakpoint: 576,
                    settings: {
                        slidesToShow: 1,
                        dots: true,
                        arrows: false
                    }
                },
            ]
        });

        $('#malls .cards-holder .slick-arrow, #propriedades .cards-holder, #variations .cards-holder .slick-arrow, #work .mini-cards-holder .slick-arrow, #our-people .mini-cards-holder .slick-arrow').css('margin-right', $('#malls .container, #propriedades .cards-holder').offset().left + 'px');









var slickOptions = {
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    dots: true,
    arrows: true,
    speed: 1000,
    autoplay: true,
    autoplaySpeed: 4000,
    fade: true,
    prevArrow: '<button type="button" class="slick-prev"></button>',
    nextArrow: '<button type="button" class="slick-next"></button>',
    responsive: [
        {
            breakpoint: 1024,
            settings: {
                speed: 150,
            }
                },
            ]
};

// Propriedades




      $('.tab-link').click(function(){
            $(this).toggleClass('open');
            $('#' + $(this).attr('id') + '.list-shoppings').slideToggle(700);
        });


//Fale Conosco
