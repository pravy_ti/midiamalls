ALTER TABLE `pages` ADD `midia_kit` VARCHAR(255) NULL AFTER `image3`;

ALTER TABLE `shoppings` ADD `class` TEXT NULL AFTER `visits`;

ALTER TABLE `shoppings` CHANGE `abl` `abl` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL;

ALTER TABLE `shoppings` ADD `site` VARCHAR(255) NULL AFTER `destaque`;

ALTER TABLE `pages` ADD `title6` VARCHAR(255) NULL AFTER `text5`, ADD `title_link6` VARCHAR(255) NULL AFTER `title6`, ADD `text6` VARCHAR(255) NULL AFTER `title_link6`;

ALTER TABLE `pages` ADD `image4` VARCHAR(255) NULL AFTER `image3`;

ALTER TABLE `pages` ADD `subtitle6` VARCHAR(255) NULL AFTER `title_link6`;

ALTER TABLE `footer` CHANGE `press_office` `press_office` VARCHAR(101) CHARACTER SET utf8 COLLATE utf8_general_ci NULL, CHANGE `press_company` `press_company` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL;

ALTER TABLE `pages` ADD `midia_kit_completo` VARCHAR(255) NULL AFTER `midia_kit`;

ALTER TABLE `pages` CHANGE `text5` `text5` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL, CHANGE `text6` `text6` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;

ALTER TABLE `shoppings` 
CHANGE COLUMN `destaque` `destaque` INT(11) NULL DEFAULT 0 ;

-- Server
-- client_max_body_size 40M; no conf do nginx 





