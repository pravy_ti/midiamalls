@include('inc.header')

<div id="inicio"></div>

<div class="banner_desktop interna" style="background-image: url({{ 'https://' . config('filesystems.disks.azure.name'). '.blob.core.windows.net/' . config('filesystems.disks.azure.container') }}/public/uploads/banners/{{$banner->desktop}})">
    <div class="container d-flex align-content-center justify-content-center align-items-center">
        <div class="col-8 -center">
            <h1 class="h1 -white align-center">
                {{$banner->desktop_title}}
            </h1>

        </div>
    </div>
</div>
<div class="banner_mobile interna" style="background-image:url({{ 'https://' . config('filesystems.disks.azure.name'). '.blob.core.windows.net/' . config('filesystems.disks.azure.container') }}/public/uploads/banners/{{$banner->mobile}})">
    <div class="container d-flex align-content-center justify-content-center align-items-center">
        <div class="col-8 -center">
            <h1 class="h1 -white align-center">
                {{$banner->mobile_title}}
            </h1>

        </div>
    </div>
</div>


        
            <div id="midia">
        <section id="brmalls" class="container-fluid -m-bottom-last-big -m-top-0">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-lg-6 -m-item">
                        <h2 class="-orange">{{$page->title}}</h2>
                        <p>{!!$page->text!!}</p>


                    </div>

                    <div class="col-9 offset-3 offset-lg-1 col-lg-5 pl-lg-0">
                        <img src="{{ 'https://' . config('filesystems.disks.azure.name'). '.blob.core.windows.net/' . config('filesystems.disks.azure.container') }}/public/uploads/pages/{{$page->image1}}" class="w-100" />
                    </div>
                </div>

                <div class="row -m-top-small">
                    <div class="col-12 col-md-6 col-xl-6 text-right -m-mobile-item">
                        <div class="element-div3"><img class="element-logo3" src="images/element.png"></div>
                        <img src="{{ 'https://' . config('filesystems.disks.azure.name'). '.blob.core.windows.net/' . config('filesystems.disks.azure.container') }}/public/uploads/pages/{{$page->image2}}" class="w-100 fix-midia-brmalls-imagem-pos-1" />
                    </div>
                    <div class="col-12 col-md-6 col-xl-6 ">
                        <h2 class="-orange">{{$page->title2}}</h2>
          
                            <p>{!! $page->text2 !!}</p>

                     
                    </div>
                </div>

            </div>
        </section>
    </div>

<style>
  section  ul { 
        margin-left: 3em!important;
        margin-bottom:35px;
        list-style:none;
    }
      section  ol { 
        margin-left: 3.5em!important;
        margin-bottom:0px;
    }
    
   section ul li::before{background-color:#f37020;}
    
section ul li::before {content: '';
display: inline-table;
vertical-align: middle;
width: 4px;
height: 21px;
background-color: #f37020;
border-bottom-left-radius: 25px;
border-top-right-radius: 25px;
-webkit-transform: rotateZ(30deg);
transform: rotateZ(30deg);
margin-right: 15px;
margin-left: 0px;
top: -.2rem;
position: relative;
    }
    
    section li {margin-bottom:0px;}
    
    
</style>


@include('inc.footer')