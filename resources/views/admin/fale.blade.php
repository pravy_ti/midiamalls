@extends('adminlte::page')

@section('title', 'Vamos conversar - midiaMalls Admin')

@section('content_header')
    <h1>Edição Vamos conversar</h1>
@stop

@section('js')
    <script src="https://cdn.ckeditor.com/4.11.4/standard/ckeditor.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.js"></script>

    <script>
        CKEDITOR.replace('address', {
            language: 'pt-br',
            customConfig: '/js/configmidia.js'
        });

        var maskBehavior = function(val) {
                return val.replace(/\D/g, '').length === 13 ? '+00 (00) 00000-0000' : '+00 (00) 0000-00009';
            },
            options = {
                onKeyPress: function(val, e, field, options) {
                    field.mask(maskBehavior.apply({}, arguments), options);
                }
            };

        $('.telefone').mask(maskBehavior, options);

    </script>
@stop

@section('content')

    <a href="fale-conosco/export" target="_blank"> <button class="btn btn-info" style="margin-left:15px;"> Exportar
            CSV</button></a>
    <div class="box"><br>

        @if (session('sucess'))
            <div class="alert alert-success">
                {{ session('sucess') }}
            </div>
        @endif
        <form id="fale-conosco" method="post" action="{{ URL::to('admin/fale-conosco/update') }}"
            enctype="multipart/form-data">
            @method('PUT')
            {{ csrf_field() }}
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Conteúdo</h3>
                    <div class="card-tools">
                        <!-- Collapse Button -->
                        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                class="fas fa-minus"></i></button>
                    </div>

                </div>

                <div class="card-body">
                    <label>Banner de Fundo</label>
                    <br>
                    @if (strlen($banner->desktop) > 3)
                    <img src="https://brmallshml.blob.core.windows.net/brmallshml/midiamalls/public/uploads/banners/{{ $banner->desktop }}" class="w100">
                    <br><br>
                    @endif
                    <input type="file" name="banner_desktop" accept="image/*">
                    <br><small>Imagem no formato PNG ou JPG, no formato de 1920x951px, com tamanho máximo de 2MB</small><br><br>


                
                    <label>E-mail</label><br>
                    <input type="text" class="w50" name="mail" value="{{ $page->title }}" placeholder="É possível inserir mais de um e-mail">
                    <br><small>Separe os emails por ponto e vírgula (;)</small><br><br>

                    <label>Contato:</label><br>
                    <input type="text" class="w50 telefone" name="phone" value="{{ $page->text }}"> <br><br>

                    <label>Endereço:</label><br><textarea class="w50" maxlength="190"
                        name="address">{{ $page->text2 }}</textarea>
                    <small>Limite de 190 Caracteres</small><br><br>

                    <label>Link para Mapa de Contato:</label><br>
                    <input type="text" class="w100" name="address_link" value="{{ $page->title2 }}"> 
                    <br><br>


                    <button type="submit" class="btn-admin-save float-left">Salvar Alterações</button>


                </div>
            </div>


            
        </form>
        <!-- /.card -->
    </div><!-- /.box -->


@endsection
