@extends('adminlte::page')

@section('title', 'Footer - midiaMalls Admin')

@section('content_header')
    <h1>Edição Footer</h1>
@stop

@section('js')
    <script src="https://cdn.ckeditor.com/4.11.4/standard/ckeditor.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.js"></script>

    <script>
        CKEDITOR.replace('adress', {
            language: 'pt-br',
            customConfig: '/js/configmidia.js'
        });
        var maskBehavior = function(val) {
                return val.replace(/\D/g, '').length === 13 ? '+00 (00) 00000-0000' : '+00 (00) 0000-00009';
            },
            options = {
                onKeyPress: function(val, e, field, options) {
                    field.mask(maskBehavior.apply({}, arguments), options);
                }
            };

        $('input.tel_number').mask(maskBehavior, options);
        $('input.tel_number2').mask(maskBehavior, options);

        /*jQuery("input.tel_number")
          .mask("+99 (99) 9999-99999")
             .onkeyup(function (event) {  
                 var target, phone, element;  
                 target = (event.currentTarget) ? event.currentTarget : event.srcElement;  
                 phone = target.value.replace(/\D/g, '');
                 element = $(target);  
                 element.unmask();  
                 if(phone.length > 12) {  
                     element.mask("+99 (99) 99999-9999");  
                 } else {  
                     element.mask("+99 (99) 9999-9999");  
                 }  
             });
         
        jQuery("input.tel_number2")
             .mask("+99 (99) 9999-9999")
             .focusout(function (event) {  
                 var target, phone, element;  
                 target = (event.currentTarget) ? event.currentTarget : event.srcElement;  
                 phone = target.value.replace(/\D/g, '');
                 element = $(target);  
                 element.unmask();  
                 if(phone.length > 10) {  
                     element.mask("+99 (99) 9999?9-9999");  
                 } else {  
                     element.mask("+99 (99) 9999-9999");  
                 }  
             }); */

    </script>
@stop


@section('content')

    <form id="footer" method="post" action="{{ URL::to('admin/footer/footerupdate') }}" enctype="multipart/form-data">
        @method('PUT')
        {{ csrf_field() }}
        <div class="box"><br>

            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Logos</h3>
                    <div class="card-tools">
                        <!-- Collapse Button -->
                        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                class="fas fa-minus"></i></button>
                    </div>

                </div>

                <div class="card-body">

                    <div class="col-4" style="float:left">
                        <label>Logo midiaMalls</label><br>

                        <img src="{{ 'https://' . config('filesystems.disks.azure.name') . '.blob.core.windows.net/' . config('filesystems.disks.azure.container') }}/public/uploads/icons/{{ $footer->logo }}"
                            style="width:198;height:42px">
                        <br><br>
                        <small>Imagem em PNG, no tamanho de 198x33px ou proporcional </small>
                        <br><br>
                        <input type="file" name="logo" accept="image/*"><br><br>
                    </div>

                    <div class="col-4" style="float:left;background-color:#ccc;">
                        <label>Logo Empresa Afiliada 1</label><br>
                        <img src="{{ 'https://' . config('filesystems.disks.azure.name') . '.blob.core.windows.net/' . config('filesystems.disks.azure.container') }}/public/uploads/icons/{{ $footer->company }}"
                            style="width:140;height:42px">
                        <br><br>
                        <small>Imagem em PNG, no tamanho máximo de 140x42px </small>
                        <br><br>
                        <input type="file" name="logo2" accept="image/*"><br><br>
                    </div>
                    <div class="col-4" style="float:left;background-color:#ccc;">
                        <label>Logo Empresa Afiliada 2</label><br>
                        <img src="{{ 'https://' . config('filesystems.disks.azure.name') . '.blob.core.windows.net/' . config('filesystems.disks.azure.container') }}/public/uploads/icons/{{ $footer->company2 }}"
                            style="width:140;height:42px">
                        <br><br>
                        <small>Imagem em PNG, no tamanho máximo de 140x42px </small>
                        <br><br>
                        <input type="file" name="logo3" accept="image/*"><br><br>
                    </div>

                </div>

                <br>
            </div>

            <br>

            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Contato</h3>
                    <div class="card-tools">
                        <!-- Collapse Button -->
                        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                class="fas fa-minus"></i></button>
                    </div>
                </div>

                <div class="card-body">
                    <label>Endereço:</label>
                    <textarea name="adress" id="adress" class="w50"
                        maxlength="190">{{ $footer->contact_adress }}</textarea><small>Limite de 190 Caracteres</small>

                    <br><br>
                    <label>Link Maps</label>
                    <input type="text" placeholder="Insira o link" name="adress_link" class="w100"
                        value="{{ $footer->contact_link }}">
                </div>

                <br>
            </div>

            <br>

            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Telefones</h3>
                    <div class="card-tools">
                        <!-- Collapse Button -->
                        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                class="fas fa-minus"></i></button>
                    </div>
                </div>


                <div class="card-body">
                    <label>Tipo de Telefone: &nbsp;&nbsp;</label>
                    <input type="text" name="tel_type" placeholder="Insira o tipo" maxlength="3"
                        style="padding:5px;margin-bottom:10px; margin-right: 20px" value="{{ $footer->phonetype }}">
                    <label> Número: &nbsp;&nbsp;</label>
                    <input type="text" name="tel_number" class="tel_number" placeholder="Ex.: 55 (11) 9999-9999"
                        style="padding:5px;margin-bottom:10px; margin-right: 20px"
                        value="{{ $footer->phonenumber }}"><br>

                    <label>Tipo de Telefone: &nbsp;&nbsp;</label>

                    <input type="text" name="tel_type2" placeholder="Insira o tipo" maxlength="3"
                        style="padding:5px;margin-bottom:10px; margin-right: 20px" value="{{ $footer->phonetype2 }}">
                    <label> Número: &nbsp;&nbsp;</label>
                    <input type="text" name="tel_number2" class="tel_number" placeholder="Ex.: 55 (11) 9999-9999"
                        style="padding:5px;margin-bottom:10px; margin-right: 20px"
                        value="{{ $footer->phonenumber2 }}"><br>

                    <label>Tipo de Telefone: &nbsp;&nbsp;</label>
                    <input type="text" name="tel_type3" placeholder="Insira o tipo" maxlength="3"
                        style="padding:5px;margin-bottom:10px; margin-right: 20px" value="{{ $footer->phonetype3 }}">
                    <label> Número: &nbsp;&nbsp;</label>
                    <input type="text" name="tel_number3" class="tel_number" placeholder="Ex.: 55 (11) 9999-9999"
                        style="padding:5px;margin-bottom:10px; margin-right: 20px"
                        value="{{ $footer->phonenumber3 }}"><br>

                    <label>Tipo de Telefone: &nbsp;&nbsp;</label>
                    <input type="text" name="tel_type4" placeholder="Insira o tipo" maxlength="3"
                        style="padding:5px;margin-bottom:10px; margin-right: 20px" value="{{ $footer->phonetype4 }}">
                    <label> Número: &nbsp;&nbsp;</label>
                    <input type="text" name="tel_number4" class="tel_number" placeholder="Ex.: 55 (11) 9999-9999"
                        style="padding:5px;margin-bottom:10px; margin-right: 20px"
                        value="{{ $footer->phonenumber4 }}"><br>

                    <label>Tipo de Telefone: &nbsp;&nbsp;</label>
                    <input type="text" name="tel_type5" placeholder="Insira o tipo" maxlength="3"
                        style="padding:5px;margin-bottom:10px; margin-right: 20px" value="{{ $footer->phonetype5 }}">
                    <label> Número: &nbsp;&nbsp;</label>
                    <input type="text" name="tel_number5" class="tel_number" placeholder="Ex.: 55 (11) 9999-9999"
                        style="padding:5px;margin-bottom:10px; margin-right: 20px"
                        value="{{ $footer->phonenumber5 }}"><br>
                </div>

            </div>

            <br>

            {{-- <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Assessoria</h3>
                    <div class="card-tools">
                        <!-- Collapse Button -->
                        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                class="fas fa-minus"></i></button>
                    </div>
                </div>

                <div class="card-body">
                    <div class="col-12" style="float:left">
                        <label>Empresa responsável:</label><br>
                        <input type="text" name="company" placeholder="Nome da Empresa" class="w25" maxlenght="50"
                            style="padding:5px;margin-bottom:10px; margin-right: 20px"
                            value="{{ $footer->press_company }}"><br>
                        <small>Limite de 50 caracteres</small><br><br>

                    </div>
                    <div class="col-6" style="float:left">



                        <h3 class="card-title">Contato 1</h3><br>
                        <input type="text" name="company_name" placeholder="Nome" class="w50" maxlength="20"
                            style="padding:5px;margin-bottom:10px; margin-right: 20px"
                            value="{{ $footer->press_name }}"><br><small>Limite de 20 Caracteres</small><br>
                        <input type="mail" name="company_mail" placeholder="Email" class="w50"
                            style="padding:5px;margin-bottom:10px; margin-right: 20px"
                            value="{{ $footer->press_mail }}"><br>
                        <input type="text" name="company_number" class="tel_number2" placeholder="Ex.: 55 (11) 9999-9999"
                            class="w50" style="padding:5px;margin-bottom:10px; margin-right: 20px"
                            value="{{ $footer->press_phone }}"><br>
                    </div>

                    <div class="col-6" style="float:left">
                        <h3 class="card-title">Contato 2</h3><br>
                        <input type="text" name="company_name2" placeholder="Nome" class="w50" maxlength="20"
                            style="padding:5px;margin-bottom:10px; margin-right: 20px"
                            value="{{ $footer->press_name2 }}"><br><small>Limite de 20 Caracteres</small><br>
                        <input type="mail" name="company_mail2" placeholder="Email" class="w50"
                            style="padding:5px;margin-bottom:10px; margin-right: 20px"
                            value="{{ $footer->press_mail2 }}"><br>
                        <input type="text" name="company_number2" class="tel_number2" placeholder="Ex.: 55 (11) 9999-9999"
                            class="w50" style="padding:5px;margin-bottom:10px; margin-right: 20px"
                            value="{{ $footer->press_phone2 }}"><br>
                    </div>


                </div>


            </div>

            <br> --}}

            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Midias Sociais</h3>
                    <div class="card-tools">
                        <!-- Collapse Button -->
                        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                class="fas fa-minus"></i></button>
                    </div>
                </div>

                <div class="card-body">
                    <label>Facebook :</label><br>
                    <input type="url" class="w100" name="facebook" style="padding:5px;margin-bottom:10px; margin-right: 20px"
                        value="{{ $footer->facebook }}"><br>
                    <label>Instagram :</label><br>
                    <input type="url" class="w100" name="instagram"
                        style="padding:5px;margin-bottom:10px; margin-right: 20px" value="{{ $footer->instagram }}"><br>
                    <label>LinkedIn :</label><br>
                    <input type="url" class="w100" name="linkedin" style="padding:5px;margin-bottom:10px; margin-right: 20px"
                        value="{{ $footer->linkedin }}"><br>
                </div>


            </div>

            <br>

            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Termos e Políticas</h3>
                    <div class="card-tools">
                        <!-- Collapse Button -->
                        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                class="fas fa-minus"></i></button>
                    </div>
                </div>

                <div class="card-body">
                    <label>Termos:</label><br>
                @if ($footer->terms == null) @else
                        <a href="{{ url("storage/uploads/files/$footer->terms") }}" target="_blank"><i
                                class="fa fa-file">
                                {{ $footer->terms }}</i></a> <br>

                    @endif

                @if ($footer->terms == null) @else
                        <input type="checkbox" name="terms"><label>&nbsp; Deletar Termos</label><br>
                    @endif
                    <input type="file" name="termos"><br><br>






                    <label>Políticas :</label><br>
                @if ($footer->policy == null) @else
                        <a href="{{ url("storage/uploads/files/$footer->policy") }}" target="_blank"><i
                                class="fa fa-file">
                                {{ $footer->policy }}</i></a> <br>

                    @endif

                @if ($footer->policy == null) @else
                        <input type="checkbox" name="policy"><label>&nbsp; Deletar Políticas</label><br>
                    @endif
                    <input type="file" name="politicas"><br><br>


                    <button type="submit" class="btn-admin-save float-left">Salvar Alterações</button>

                </div>
                

            </div>


            {{-- <button type="submit" class="btn-admin-save float-left">Salvar Alterações</button> --}}
        </div>
    </form>
    <!-- /.card -->
    <!-- /.box -->


@endsection
