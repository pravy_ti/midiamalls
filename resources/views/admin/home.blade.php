@extends('adminlte::page')

@section('title', 'Home - midiaMalls Admin')

@section('js')

    <script src="https://cdn.ckeditor.com/4.11.4/standard/ckeditor.js"></script>

    <script>
        CKEDITOR.replace('content_text', {
            language: 'pt-br',
            customConfig: '/js/configmidia.js'
        });
        CKEDITOR.replace('content_text2', {
            language: 'pt-br',
            customConfig: '/js/configmidia.js'
        });
        CKEDITOR.replace('content_text4', {
            language: 'pt-br',
            customConfig: '/js/configmidia.js'
        });

        CKEDITOR.replace('content_text5', {
            language: 'pt-br',
            customConfig: '/js/configmidia.js'
        });

        CKEDITOR.replace('content_text6', {
            language: 'pt-br',
            customConfig: '/js/configmidia.js'
        });

        $(document).ready(function() {
          var base_url = "{{ route('site.home') }}";
            
          $('#tabela').change(function() {
              if(this.checked) {
                window.location.replace(base_url + "/admin/home/deletepreco");
              }
                      
          });
        });

    </script>
@stop

@section('content_header')
    <h1>Edição Home</h1>
@stop



@section('content')
    <div class="box"><br>

        @if (session('sucess'))
            <div class="alert alert-success">
                {{ session('sucess') }}
            </div>
        @endif

        <form id="home" method="post" action="{{ URL::to('admin/home/update') }}" enctype="multipart/form-data">
            @method('PUT')
            {{ csrf_field() }}
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Banner</h3>
                    <div class="card-tools">
                        <!-- Collapse Button -->
                        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                class="fas fa-minus"></i></button>
                    </div>

                </div>

                <div class="card-body">
                    <label>Banner Desktop</label>
                    <br>
                    <img src="{{ 'https://' . config('filesystems.disks.azure.name') . '.blob.core.windows.net/' . config('filesystems.disks.azure.container') }}/public/uploads/banners/{{ $banner->desktop }}"
                        class="w100" style="max-height:830px">
                    <br><br>
                    <input type="file" name="banner_desktop" accept="image/*">
                    <br>
                    <small>Imagem no formato PNG ou JPG, no formato de 1920x830px, com tamanho máximo de 2MB</small>
                    <br><br>

                    <label>Texto Banner Desktop</label>
                    <br>
                    <input type="text" placeholder="Texto do Banner" class="banner_desk_title" maxlength="76"
                        name="banner_desk_title" value="{{ $banner->desktop_title }}">
                    <br>
                    <small>Máximo de 76 Caracteres</small><br>
                    <br>

                    <label>Banner Mobile</label>
                    <br>
                    <img
                        src="{{ 'https://' . config('filesystems.disks.azure.name') . '.blob.core.windows.net/' . config('filesystems.disks.azure.container') }}/public/uploads/banners/{{ $banner->mobile }}">
                    <br><br>
                    <input type="file" name="banner_mobile" accept="image/*"><br><small>Imagem no formato PNG ou JPG, no
                        formato de 375x733px, com tamanho máximo de 2MB</small><br><br>

                    <label>Texto Banner Mobile</label><br>
                    <input type="text" placeholder="Texto do Banner" class="banner_mobile_title" maxlength="76"
                        name="banner_mobile_title" value="{{ $banner->mobile_title }}">
                    <br> <small>Máximo de 76 Caracteres</small><br><br>
                </div>


            </div>

            <br>
            <!-- A midiamalls -->

            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Sobre nós</h3>
                    <div class="card-tools">
                        <!-- Collapse Button -->
                        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                class="fas fa-minus"></i></button>
                    </div>

                </div>

                <div class="card-body">
                    <label>Titulo:</label><br> <input type="text" class="w50" maxlength="50" name="content_title"
                        value="{{ $page->title }}"><br>
                    <small>Máximo de 50 Caracteres</small><br><br>
                    <label>Link Titulo:</label><br> <input type="text" class="w50" name="content_link"
                        value="{{ $page->title_link }}"><br><br>
                    <label>Texto:</label><br><textarea class="w50" maxlength="190"
                        name="content_text">{{ $page->text }}</textarea>
                    <small>Máximo de 190 Caracteres</small><br><br>

                    <label>Titulo 2:</label><br> <input type="text" class="w50" maxlength="50" name="content_title2"
                        value="{{ $page->title2 }}"><br>
                    <small>Máximo de 76 Caracteres</small><br><br>
                    <label>Texto 2:</label><br><textarea class="w50" maxlength="190"
                        name="content_text2">{{ $page->text2 }}</textarea>
                    <small>Máximo de 190 Caracteres</small><br><br>


                </div>


            </div>
            <!-- Fim de A midiamalls -->

            <br>

            <!-- Por que Anunciar -->
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Por que anunciar?</h3>
                    <div class="card-tools">
                        <!-- Collapse Button -->
                        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                class="fas fa-minus"></i></button>
                    </div>

                </div>

                <div class="card-body">
                    <label>Titulo:</label><br> <input type="text" class="w50" maxlength="50" name="content_title3"
                        value="{{ $page->title3 }}"><br>
                    <small>Máximo de 76 Caracteres</small><br><br>
                    <label>Link Titulo:</label><br> <input type="text" class="w50" name="content_link3"
                        value="{{ $page->title_link3 }}"><br><br>
                    <label>Texto:</label><br><textarea class="w50" maxlength="65"
                        name="content_text3">{{ $page->text3 }}</textarea><br>
                    <small>Máximo de 190 Caracteres</small><br><br>

                    <div class="col-4" style="float:left;margin-bottom:20px">
                        <label>Icone 1</label><br>


                        @if ($announce->icon != null)<img
                                src="{{ 'https://' . config('filesystems.disks.azure.name') . '.blob.core.windows.net/' . config('filesystems.disks.azure.container') }}/public/uploads/icons/{{ $announce->icon }}"
                            style="background-color:#F06F30;width:80px;height:80px">@else @endif
                        <br>
                        <input type="text" maxlength="110" name="icon_name" class="w75"
                            value="{{ $announce->title }}"><br>
                        <small>Máximo de 110 Caracteres</small><br>
                        <input type="file" name="icon_file" accept="image/*"><br>
                        <small>
                            Ícone brancos com 80x80px em formato PNG</small><br>

                    </div>

                    <div class="col-4" style="float:left;margin-bottom:20px">
                        <label>Icone 2</label><br>
                        @if ($announce2->icon != null)<img
                                src="{{ 'https://' . config('filesystems.disks.azure.name') . '.blob.core.windows.net/' . config('filesystems.disks.azure.container') }}/public/uploads/icons/{{ $announce2->icon }}"
                            style="background-color:#F06F30;width:80px;height:80px">@else @endif
                        <br>
                        <input type="text" maxlength="110" name="icon_name2" class="w75"
                            value="{{ $announce2->title }}"><br>
                        <small>Máximo de 110 Caracteres</small><br>
                        <input type="file" name="icon_file2" accept="image/*"><br>

                        <small>Ícone brancos com 80x80px em formato PNG</small><br>

                    </div>

                    <div class="col-4" style="float:left;margin-bottom:20px">
                        <label>Icone 3</label><br>
                        @if ($announce3->icon != null)<img
                                src="{{ 'https://' . config('filesystems.disks.azure.name') . '.blob.core.windows.net/' . config('filesystems.disks.azure.container') }}/public/uploads/icons/{{ $announce3->icon }}"
                            style="background-color:#F06F30;width:80px;height:80px">@else @endif
                        <br>
                        <input type="text" maxlength="110" name="icon_name3" class="w75"
                            value="{{ $announce3->title }}"><br>
                        <small>Máximo de 110 Caracteres</small><br>
                        <input type="file" name="icon_file3" accept="image/*"><br>
                        <small>Ícone brancos com 80x80px em formato PNG</small><br>

                    </div>
                    <div class="col-4" style="float:left;">
                        <label>Icone 4</label><br>
                        @if ($announce4->icon != null)<img
                                src="{{ 'https://' . config('filesystems.disks.azure.name') . '.blob.core.windows.net/' . config('filesystems.disks.azure.container') }}/public/uploads/icons/{{ $announce4->icon }}"
                            style="background-color:#F06F30;width:80px;height:80px">@else @endif
                        <br>
                        <input type="text" maxlength="110" name="icon_name4" class="w75"
                            value="{{ $announce4->title }}"><br>
                        <small>Máximo de 110 Caracteres</small><br>
                        <input type="file" name="icon_file4" accept="image/*"><br>
                        <small>Ícone brancos com 80x80px em formato PNG</small><br>

                    </div>

                    <div class="col-4" style="float:left;">
                        <label>Icone 5</label><br>
                        @if ($announce5->icon != null)<img
                                src="{{ 'https://' . config('filesystems.disks.azure.name') . '.blob.core.windows.net/' . config('filesystems.disks.azure.container') }}/public/uploads/icons/{{ $announce5->icon }}"
                            style="background-color:#F06F30;width:80px;height:80px">@else @endif
                        <br>
                        <input type="text" maxlength="110" name="icon_name5" class="w75"
                            value="{{ $announce5->title }}"><br>
                        <small>Máximo de 110 Caracteres</small><br>
                        <input type="file" name="icon_file5" accept="image/*"><br>
                        <small>Ícone brancos com 80x80px em formato PNG</small><br>
                    </div>

                    <div class="col-4" style="float:left;">
                        <label>Icone 6</label><br>
                        @if ($announce6->icon != null)<img
                                src="{{ 'https://' . config('filesystems.disks.azure.name') . '.blob.core.windows.net/' . config('filesystems.disks.azure.container') }}/public/uploads/icons/{{ $announce6->icon }}"
                            style="background-color:#F06F30;width:80px;height:80px">@else @endif
                        <br>
                        <input type="text" maxlength="110" name="icon_name6" class="w75"
                            value="{{ $announce6->title }}"><br>
                        <small>Máximo de 110 Caracteres</small><br>
                        <input type="file" name="icon_file6" accept="image/*"><br>
                        <small>Ícone brancos com 80x80px em formato PNG</small><br><br>

                    </div>

                    <div class="col-12" style="float:left;">
                        <br>
                        <label>Link externo midiaKit completo:</label><br>
                        <input type="text" class="w100" maxlength="255" name="midia_kit_completo" value="{{ $page->midia_kit_completo }}"><br>
                        <small>Link externo de nuvem (ex one drive, google drive, etc)</small><br><br>
                    </div>


                </div>

                <br>
            </div>

            <!-- Fim de Por que Anunciar -->

            <br>

            <!-- Cobertura -->
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Cobertura</h3>
                    <div class="card-tools">
                        <!-- Collapse Button -->
                        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                class="fas fa-minus"></i></button>
                    </div>

                </div>

                <div class="card-body">
                    <label>Titulo:</label><br> <input type="text" maxlength="50" class="w50" name="content_title4"
                        value="{{ $page->title4 }}"><br>
                    <small>Máximo de 50 Caracteres</small>
                    <br><br>
                    <label>Link Titulo:</label><br> <input type="text" class="w50" name="content_link4"
                        value="{{ $page->title_link4 }}">
                    <br><br>
                    <label>Texto:</label><br><textarea maxlength="190" class="w50"
                        name="content_text4">{{ $page->text4 }}</textarea><br>
                    <small>Máximo de 190 Caracteres</small><br><br>

                </div>
            </div>
            <!-- Fim de Cobertura -->

            <br>

            <!-- Formatos disponíveis -->
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Formatos disponíveis</h3>
                    <div class="card-tools">
                        <!-- Collapse Button -->
                        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                class="fas fa-minus"></i></button>
                    </div>

                </div>

                <div class="card-body">
                    <label>Titulo:</label><br> <input type="text" maxlength="50" class="w50" name="content_title5"
                        value="{{ $page->title5 }}"><br>
                    <small>Máximo de 50 Caracteres</small><br><br>

                    <label>Subtitulo:</label><br> <input type="text" class="w50" name="content_link5"
                        value="{{ $page->title_link5 }}">
                    <br><br>
                    
                    <label>Texto:</label><br><textarea maxlength="190" class="w50"
                        name="content_text5">{{ $page->text5 }}</textarea><br>
                    <small>Máximo de 130 Caracteres</small><br><br>

                    <a href="{{ URL::to('admin/propriedades') }}" class="btn-admin-save float-left">Editar formatos
                        disponíveis</a>
                </div>
            </div>
            <!-- Fim de Formatos disponíveis -->

            <!-- Propriedades -->

            {{-- <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Propriedades</h3>
                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                class="fas fa-minus"></i></button>
                    </div>

                </div>

                <div class="card-body">
                    <label>Titulo:</label><br> <input type="text" class="w50" maxlength="50" name="content_title5"
                        value="{{ $page->title5 }}"><br>
                    <small>Máximo de 50 Caracteres</small><br>
                    <label>Link Titulo:</label><br> <input type="text" class="w50" name="content_link5"
                        value="{{ $page->title_link5 }}"><br>

                </div>


            </div> --}}
            <!-- Fim de Propriedades -->

            <br>

            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Imagens</h3>
                    <div class="card-tools">
                        <!-- Collapse Button -->
                        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                class="fas fa-minus"></i></button>
                    </div>

                </div>

                <div class="card-body">
                    <label>Imagem:</label>
                    <br>
                    <small>Imagem no formato PNG ou JPG, no formado de 548x747px, com tamanho máximo de 2MB</small>
                    <br>
                    <input type="file" name="midia1" accept="image/*">
                    <br><br>
                    @if (strlen($page->image1) > 3)
                        <img src="{{ 'https://' . config('filesystems.disks.azure.name') . '.blob.core.windows.net/' . config('filesystems.disks.azure.container') }}/public/uploads/pages/{{ $page->image1 }}"
                            style="width:548px;height:747px">
                        <br><br>
                    @endif


                    <label>Imagem 2:</label>
                    <br>
                    <small>Imagem no formato PNG ou JPG, no formado de 554x406px, com tamanho máximo de 2MB</small>
                    <br>
                    <input type="file" name="midia2" accept="image/*">
                    <br><br>
                    @if (strlen($page->image2) > 3)
                        <img src="{{ 'https://' . config('filesystems.disks.azure.name') . '.blob.core.windows.net/' . config('filesystems.disks.azure.container') }}/public/uploads/pages/{{ $page->image2 }}"
                            style="width:554px;height:406px">
                        <br><br>
                    @endif


                    <label>Imagem 3:</label>
                    <br>
                    <small>Imagem no formato PNG ou JPG, no formado de 922x1088px, com tamanho máximo de 2MB</small>
                    <br>
                    <input type="file" name="midia3" accept="image/*">
                    <br><br>
                    @if (strlen($page->image3) > 3)
                        <img src="{{ 'https://' . config('filesystems.disks.azure.name') . '.blob.core.windows.net/' . config('filesystems.disks.azure.container') }}/public/uploads/pages/{{ $page->image3 }}"
                            style="width:554px;height:406px">
                        <br><br>
                    @endif


                    <label>Imagem 4:</label>
                    <br>
                    <small>Imagem no formato PNG ou JPG, no formado de 922x1088px, com tamanho máximo de 2MB</small>
                    <br>
                    <input type="file" name="midia4" accept="image/*">
                    <br><br>
                    @if (strlen($page->image4) > 3)
                        <img src="{{ 'https://' . config('filesystems.disks.azure.name') . '.blob.core.windows.net/' . config('filesystems.disks.azure.container') }}/public/uploads/pages/{{ $page->image4 }}"
                            style="width:554px;height:406px">
                        <br><br>
                    @endif

                </div>
            </div>

            <br>

            <!-- Midia Kit -->
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Mídia Kit</h3>
                    <div class="card-tools">
                        <!-- Collapse Button -->
                        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                class="fas fa-minus"></i></button>
                    </div>

                </div>

                <div class="card-body">
                    <label>Titulo:</label><br> <input type="text" maxlength="50" class="w50" name="content_title6"
                        value="{{ $page->title6 }}"><br>
                    <small>Máximo de 50 Caracteres</small><br><br>


                    <label>Subitulo:</label><br> <input type="text" maxlength="50" class="w50" name="subtitle6"
                        value="{{ $page->subtitle6 }}"><br><br>
                    {{-- <small>Máximo de 50 Caracteres</small><br> --}}


                    <label>Descrição:</label><br><textarea maxlength="190" class="w50"
                        name="content_text6">{{ $page->text6 }}</textarea><br>
                    <small>Máximo de 130 Caracteres</small><br><br>


                    <label>Link externo midiaKit nacional:</label><br> <input type="text" class="w100" maxlength="255" name="midia_kit" value="{{ $page->midia_kit }}"><br>
                    <small>Link externo de nuvem (ex one drive, google drive, etc)</small><br><br>

                    <label>Tabela de preços:</label><br>
                    @if (strlen($page->title_link6) > 3)
                        <a href="{{ 'https://' . config('filesystems.disks.azure.name') . '.blob.core.windows.net/' . config('filesystems.disks.azure.container') }}/public/uploads/pages/{{ $page->title_link6 }}"
                            target="_blank"><i class="fa fa-file"> {{ $page->title_link6 }}</i></a>
                        <br>
                        <input type="checkbox" name="tabela" id="tabela"><label>&nbsp; Deletar Tabela de Preços</label>
                        <br>
                    @endif
                    <input type="file" name="content_link6"><br>
                    <small>Upload com tamanho de até 10Mb e extensão PDF</small>
                    <br><br>

                    <button type="submit" class="btn-admin-save float-left" form="home">Salvar Alterações</button>
                </div>
            </div>

            <!-- Fim de Midia Kit -->

            <br>

            <!-- Vamos conversar -->
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Vamos conversar</h3>
                    <div class="card-tools">
                        <!-- Collapse Button -->
                        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                class="fas fa-minus"></i></button>
                    </div>

                </div>

                <div class="card-body">
                    <br>
                    <a href="{{ URL::to('admin/fale-conosco') }}" class="btn-admin-save float-left">Editar Vamos conversar</a>
                </div>
            </div>
            <!-- Fim de Vamos conversar -->

            <br>

            <!-- Footer -->
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Footer</h3>
                    <div class="card-tools">
                        <!-- Collapse Button -->
                        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                class="fas fa-minus"></i></button>
                    </div>

                </div>

                <div class="card-body">
                    <br>
                    <a href="{{ URL::to('admin/footer') }}" class="btn-admin-save float-left">Editar Footer</a>
                </div>
            </div>
            <!-- Fim de Footer -->

            
        </form>
        <!-- /.card -->
    </div><!-- /.box -->


@endsection
