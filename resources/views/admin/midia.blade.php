@extends('adminlte::page')

@section('title', 'midiaMalls - midiaMalls Admin')

@section('content_header')
    <h1>Edição A midiaMalls</h1>
@stop

@section ('js')
<script src="https://cdn.ckeditor.com/4.11.4/standard/ckeditor.js"></script>

<script>
    CKEDITOR.replace( 'content_text', {language: 'pt-br', customConfig:'/js/configmidia.js'});
CKEDITOR.replace( 'content_text2', {language: 'pt-br', customConfig:'/js/configmidia.js'});
</script>
@stop

@section('content')
    <div class="box"><br>

		@if (session('sucess'))
          <div class="alert alert-success">
              {{ session('sucess') }}
          </div>
      	@endif
        <form id="midia" method="post" action="{{URL::to('admin/midia/update')}}" enctype="multipart/form-data">
    @method('PUT')
  {{ csrf_field() }}
<div class="card">
  <div class="card-header">
    <h3 class="card-title">Banner</h3>
                      <div class="card-tools">
      <!-- Collapse Button -->
      <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
    </div>

  </div>

  <div class="card-body">
       <label>Banner Desktop</label><br><img src="{{ 'https://' . config('filesystems.disks.azure.name'). '.blob.core.windows.net/' . config('filesystems.disks.azure.container') }}/public/uploads/banners/{{$banner->desktop}}" class="w100"><br><input type="file" name="banner_desktop"><br><small>Imagem no formato PNG ou JPG, no formato de 1920x531px, com tamanho máximo de 2MB</small><br><br>
      <label>Texto de Banner Desktop</label><br>
      <input type="text" placeholder="Texto do Banner" class="banner_desk_title" maxlength="76" name="banner_desk_title" value="{{$banner->desktop_title}}"> <br><small>Até 76 caracteres</small>
      <br><br>
       <label>Banner Mobile</label><br><img src="{{ 'https://' . config('filesystems.disks.azure.name'). '.blob.core.windows.net/' . config('filesystems.disks.azure.container') }}/public/uploads/banners/{{$banner->mobile}}"><br><input type="file" name="banner_mobile"><br><small>Imagem no formato PNG ou JPG, no formato de 375x480px, com tamanho máximo de 2MB</small><br><br>
      <label>Texto de Banner Mobile</label><br>
      <input type="text" placeholder="Texto do Banner" class="banner_mobile_title" maxlength="76" name="banner_mobile_title" value="{{$banner->mobile_title}}"><br><small>Até 76 caracteres</small>
  </div>


</div>
        
<div class="card">
  <div class="card-header">
    <h3 class="card-title">Conteúdo</h3>
                      <div class="card-tools">
      <!-- Collapse Button -->
      <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
    </div>

  </div>

  <div class="card-body">
      <label>Titulo:</label><br> <input type="text" class="w50" maxlength="50" name="content_title" value="{{$page->title}}"><br><small>Até 50 Caracteres</small><br>
      <label>Texto:</label><br><textarea class="w50" maxlength="190" name="content_text">{{$page->text}}</textarea><br><small>Até 190 Caracteres</small><br><br><br>

      <label>Titulo 2:</label><br> <input type="text" class="w50" maxlength="50" name="content_title2" value="{{$page->title2}}"><br><small>Até 50 Caracteres</small><br><br>
      <label>Texto 2:</label><br><textarea class="w50" maxlength="190" name="content_text2">{{$page->text2}}</textarea><br><small>Até 190 Caracteres</small><br><br><br>
          
          
          
  </div>


</div>
        
        <div class="card">
  <div class="card-header">
    <h3 class="card-title">Imagens</h3>
                      <div class="card-tools">
      <!-- Collapse Button -->
      <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
    </div>

  </div>

  <div class="card-body">
      <label>Imagem Desktop:</label><br> <input type="file" name="midia1"><br><small>Imagem no formato PNG ou JPG, no formato de 695x641px, com tamanho máximo de 2MB</small><br>
      <img src="{{ 'https://' . config('filesystems.disks.azure.name'). '.blob.core.windows.net/' . config('filesystems.disks.azure.container') }}/public/uploads/pages/{{$page->image1}}"><br><br>

      <label>Imagem Desktop 2 / Mobile:</label><br> <input type="file" name="midia2"><br><small>Imagem no formato PNG ou JPG, no formato de 831x609px, com tamanho máximo de 2MB</small><br>
      <img src="{{ 'https://' . config('filesystems.disks.azure.name'). '.blob.core.windows.net/' . config('filesystems.disks.azure.container') }}/public/uploads/pages/{{$page->image2}}"><br><br>
          
          
  </div>


</div>
        
        <button type="submit" class="btn-admin-save">Salvar Alterações</button>
        </form>
<!-- /.card -->
	</div><!-- /.box -->


@endsection
