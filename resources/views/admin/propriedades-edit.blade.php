@extends('adminlte::page')

@section('title', 'Propriedades - midiaMalls Admin')

@section('content_header')
    <h1>Editar Formatos disponíveis</h1>
@stop
@section('js')

    {{-- <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script>
        $(document).ready(function() {
            $('.dataTable').dataTable({
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.20/i18n/Portuguese-Brasil.json"
                }
            });
        });

        @foreach ($shoppings as $shop)
        
        
            $(document).ready(function() {
            $('#check{{ $shop->id }}').change(function() {
            $('#checkshow{{ $shop->id }}').toggle();
            });
            });
        @endforeach

    </script> --}}
@stop


@section('content')

    <form id="home" method="post" action="{{ URL::to('admin/propriedades/item/update') }}" enctype="multipart/form-data">
        @method('PUT')
        {{ csrf_field() }}
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Editar {{ $props['name'] }}</h3>
                <div class="card-tools">
                    <!-- Collapse Button -->
                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                            class="fas fa-minus"></i></button>
                </div>

            </div>

            <div class="card-body">
                <img class="w100" style="max-height:550px;"
                    src="{{ 'https://' . config('filesystems.disks.azure.name') . '.blob.core.windows.net/' . config('filesystems.disks.azure.container') }}/public/uploads/propriedades/{{ $props['imagem'] }}"><br>
                <label>Banner <small>Seguir padrão de 1920x550px ou proporcional</small></label><br>
                <input type="file" name="bannerprop" accept="image/*"> <br><br>


                <div class="col-4" style="float:left;">
                    <label>Nome do formato:</label><br>
                    <input type="text" class="w75" name="propriedade" value="{{ $props['name'] }}" required><br><br>

                </div>
                <div class="col-4" style="float:left;">
                    <img src="{{ 'https://' . config('filesystems.disks.azure.name') . '.blob.core.windows.net/' . config('filesystems.disks.azure.container') }}/public/uploads/icons/{{ $props['icon'] }}"
                        style="width:125px;height:125px;"><br>
                    <label>Icone <br><small>Icone azul (125x125px ou proporcional)</small></label><br>
                    <input type="file" name="icon" accept="image/*"> <br><br>
                </div>
                <div class="col-4" style="float:left;">
                    <img src="{{ 'https://' . config('filesystems.disks.azure.name') . '.blob.core.windows.net/' . config('filesystems.disks.azure.container') }}/public/uploads/icons/{{ $props['iconh'] }}"
                        style="width:125px;height:125px;"><br>
                    <label>Icone Hover<br> <small>Icone laranja (125x125px ou proporcional)</small></label><br>
                    <input type="file" name="iconh" accept="image/*"> <br><br>
                </div>

                <input type="hidden" name="idprop" value="{{ $props['id'] }}">

                <button type="submit" class="btn-admin-save float-left">Atualizar Formato</button><br><br><br>
            </div>


        </div>

        <?php
        /*
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Shoppings</h3>
                <div class="card-tools">
                    <!-- Collapse Button -->
                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                            class="fas fa-minus"></i></button>
                </div>

            </div>

            <div class="card-body">
                <h3>Selecione os Shoppings que possuem a propriedade</h3><br><br>

                @foreach ($shoppings as $shop)


                    <div class="col-3" style="float:left;">


                        <input type="checkbox" @foreach ($shopro as $pro)  @if ($pro->name==$props['name'])
                        @if ($pro->id_shopping == $shop->id)
                        checked @else @endif

                        @endif
                @endforeach


                id="check{{ $shop->id }}" name="shop_prop[]" value={{ $shop->id }}>

                <label>{{ $shop->name }}</label><br>
                @foreach ($shopro as $pro)
                    @if ($pro->id == $shop->id)
                        @if ($pro->name == $props['name'])
                            <input type="hidden" name="shopup[]" value="{{ $pro->idprop }}">
                    @endif @else
                    @endif
                @endforeach

            </div>



            @endforeach
            */
            ?>




            <!--  @foreach ($shoppings as $shop) @foreach ($shopro as $pro)


           
        <div class="col-3" style="float:left;">
            
            
        <input type="checkbox"
    @if ($pro->id == $shop->id) checked @else @endif
               id="check{{ $shop->id }}" name="shop_prop[]" value={{ $shop->id }}>
            
          <label>{{ $shop->name }}</label><br>
                  
    @if ($pro->id == $shop->id)<input type="text" name="shopup" value="{{ $pro->idprop }}">@else @endif

    <input type="hidden" name="shopid" value="{{ $shop->id }}">
                
            <br><br>
                
            </div>


          @endforeach
          
          @endforeach -->

        </div>


        </div>
        {{-- <button  type="submit" class="btn-admin-save pull-left">Atualizar Formato</button><br><br><br> --}}
    </form>
@endsection
