@extends('adminlte::page')

@section('title', 'Propriedades - midiaMalls Admin')

@section('content_header')
    <h1>Adicionar Formatos disponíveis<h1>
        @stop
        @section('js')

            {{-- <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script>
        $(document).ready(function () {$('.dataTable').dataTable({"language": {"url": "//cdn.datatables.net/plug-ins/1.10.20/i18n/Portuguese-Brasil.json"}});});
        
    </script> --}}
        @stop


        @section('content')
            <form id="novapropriedade" method="post" action="{{ URL::to('admin/propriedades/novo/inserir') }}"
                enctype="multipart/form-data">
                @method('PUT')
                {{ csrf_field() }}
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Novo Formatos disponíveis</h3>
                        <div class="card-tools">
                            <!-- Collapse Button -->
                            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                    class="fas fa-minus"></i></button>
                        </div>

                    </div>

                    <div class="card-body">
                        <label>Banner <small>Seguir padrão de 1920x550px ou proporcional</small></label><br>
                        <input type="file" name="bannerprop" required accept="image/*"> <br><br>



                        <div class="col-4" style="float:left;">
                            <label>Nome do formato:</label><br>
                            <input type="text" class="w75" name="propriedade" required><br><br>

                        </div>
                        <div class="col-4" style="float:left;">
                            <label>Icone</label><br> <small>Icone azul (125x125px ou proporcional)</small><br>
                            <input type="file" name="icon" required accept="image/*"> <br><br>
                        </div>
                        <div class="col-4" style="float:left;">
                            <label>Icone Hover </label><br><small>Icone laranja (125x125px ou
                                proporcional)</small><br>
                            <input type="file" name="iconh" required accept="image/*"> <br><br>
                        </div>

                        <button type="submit" class="btn-admin-save">Inserir Propriedade</button><br><br><br>


                    </div>


                </div>

                {{-- <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Shoppings</h3>
                        <div class="card-tools">
                            <!-- Collapse Button -->
                            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                    class="fas fa-minus"></i></button>
                        </div>

                    </div>

                    <div class="card-body">
                        <h3>Selecione os Shoppings que possuem a nova propriedade</h3><br><br>

                        @foreach ($shoppings as $shop)
                            <div class="col-3" style="float:left;">

                                <input type="checkbox" id="check{{ $shop->id }}" name="shop_prop[]"
                                    value={{ $shop->id }}>
                                <label>{{ $shop->name }}</label><br>
                                <div id="checkshow{{ $shop->id }}" style="display:none;">
                                    <input type="file" name="shop_image[]"> <input type="hidden" name="shopid"
                                        value="{{ $shop->id }}">
                                    <br><br>
                                </div>
                            </div>
                        @endforeach



                    </div>
                </div> --}}

                {{-- <button type="submit" class="btn-admin-save">Inserir Propriedade</button><br><br><br> --}}
            </form>
        @endsection
