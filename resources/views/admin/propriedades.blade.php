@extends('adminlte::page')

@section('title', 'Propriedades - midiaMalls Admin')

@section('content_header')
    <h1>Edição de Formatos disponíveis</h1>
@stop

@section('css')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
@stop

@section('js')

<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19//js/dataTables.bootstrap4.min.js"></script>

<script>

    $(document).ready(function() {
        $('.dataTable').dataTable({
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.20/i18n/Portuguese-Brasil.json"
            }
        });
    });
</script>

@stop



@section('content')
    {{-- <form id="home" method="post" action="{{ URL::to('admin/propriedades/update') }}" enctype="multipart/form-data">
        @method('PUT')
        {{ csrf_field() }}

        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Banner</h3>
                <div class="card-tools">
                    <!-- Collapse Button -->
                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                            class="fas fa-minus"></i></button>
                </div>

            </div>

            <div class="card-body">

                <label>Banner Desktop</label><br><img
                    src="{{ 'https://' . config('filesystems.disks.azure.name') . '.blob.core.windows.net/' . config('filesystems.disks.azure.container') }}/public/uploads/banners/{{ $banner->desktop }}"
                    class="w100"><br><input type="file" name="banner_desktop" accept="image/*"><br><small>Imagem no formato
                    PNG ou JPG, no formato de 1920x530px, com tamanho máximo de 2MB</small><br><br>
                <label>Texto de Banner Desktop<label><br></label>
                    <input type="text" placeholder="Texto do Banner" class="banner_desk_title" maxlength="76"
                        name="banner_desk_title" value="{{ $banner->desktop_title }}">
                    <br><small>Até 76 caracteres</small><br>
                    <br><br>
                    <label>Banner Mobile</label><br><img
                        src="{{ 'https://' . config('filesystems.disks.azure.name') . '.blob.core.windows.net/' . config('filesystems.disks.azure.container') }}/public/uploads/banners/{{ $banner->mobile }}"
                        style="max-width:375px;max-height:482px;"><br><input type="file" name="banner_mobile"
                        accept="image/*"><br>
                    <small>Imagem no formato PNG ou JPG, no formato de 375x480px, com tamanho máximo de 2MB</small><br>
                    <label>Texto de Banner Mobile </label><br>
                    <input type="text" placeholder="Texto do Banner" class="banner_mobile_title" maxlength="76"
                        name="banner_mobile_title" value="{{ $banner->mobile_title }}">
                    <br><small>Até 76 caracteres</small><br>
            </div>


        </div>


        <button type="submit" class="btn-admin-save">Salvar Alterações</button>
    </form><br><br><br><br> --}}

    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Formatos disponíveis</h3>
            <div class="card-tools">
                <!-- Collapse Button -->
                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
            </div>

        </div>


        <div class="card-body">
            <a href="propriedades/novo"> <button class="btn-info">Adicionar Propriedade</button></a><br><br><br>

            <div id="example2_wrapper" class="dataTables_wrapper dt-bootstrap4">
                <div class="row">
                    <div class="col-sm-12 col-md-6"></div>
                    <div class="col-sm-12 col-md-6"></div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <table class="table table-bordered table-hover dataTable">
                            <thead>
                                <tr>
                                    <th>Propriedade</th>
                                    <th>Ícone</th>
                                    <th>Ações</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($propriedades as $prop)


                                    <tr>
                                        <td>{{ $prop->name }}</td>

                                        <td><img src="{{ 'https://' . config('filesystems.disks.azure.name') . '.blob.core.windows.net/' . config('filesystems.disks.azure.container') }}/public/uploads/icons/{{ $prop->icon }}"
                                                style="width:125px;height:125px"></td>

                                        <td>
                                            <button type="button" class="btn btn-danger" data-toggle="modal"
                                                data-target="#modal-sm{{ $prop->id }}">
                                                <i class="fa fa-window-close"></i>
                                            </button>
                                            <a href="propriedades/{{ $prop->id }}"><button type="button"
                                                    class="btn btn-info"><i class="fa fa-pen"></i></button></a>
                                        </td>
                                    </tr>


                                    <div class="modal fade" id="modal-sm{{ $prop->id }}" style="display: none;"
                                        aria-hidden="true">
                                        <div class="modal-dialog modal-sm">
                                            <div class="modal-content">
                                                <form id="home" method="post"
                                                    action="{{ URL::to('admin/propriedades/delete') }}"
                                                    enctype="multipart/form-data">
                                                    @method('PUT')
                                                    {{ csrf_field() }}
                                                    <div class="modal-header">
                                                        <h4 class="modal-title">Deletar</h4>
                                                        <button type="button" class="close" data-dismiss="modal"
                                                            aria-label="Close">
                                                            <span aria-hidden="true">×</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <p>Tem certeza que deseja deletar<br>
                                                            {{ $prop->name }} ?
                                                        </p>
                                                        <input type="hidden" name="idshop" value="{{ $prop->id }}">
                                                    </div>
                                                    <div class="modal-footer justify-content-between">
                                                        <button type="button" class="btn btn-default"
                                                            data-dismiss="modal">Fechar</button>
                                                        <button type="submit" class="btn btn-danger">Sim</button>
                                                    </div>
                                                </form>
                                            </div>
                                            <!-- /.modal-content -->
                                        </div>
                                        <!-- /.modal-dialog -->
                                    </div>




                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>Propriedade</th>
                                    <th>Ícone</th>
                                    <th>Ações</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>

            </div>



        </div>


    </div>
@endsection
