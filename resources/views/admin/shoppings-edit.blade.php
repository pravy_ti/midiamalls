@extends('adminlte::page')

@section('title', 'Cobertura - midiaMalls Admin')

@section('content_header')
    <h1>Editar Cobertura</h1>
@stop
@section('js')

<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script>
        $(document).ready(function () {$('.dataTable').dataTable({"language": {"url": "//cdn.datatables.net/plug-ins/1.10.20/i18n/Portuguese-Brasil.json"}});});
    </script>
@stop


@section('content')

<form id="home" method="post" action="{{URL::to('admin/nossos-shoppings/novo/inserir')}}" enctype="multipart/form-data">
    @method('PUT')
  {{ csrf_field() }}
<div class="card">
  <div class="card-header">
    <h3 class="card-title">Editar {{ $shop->name }}</h3>
                      <div class="card-tools">
      <!-- Collapse Button -->
      <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
    </div>

  </div>

  <div class="card-body">
      
       <label>Imagem do Shopping <small>Seguir padrão de 400x334px</small></label><br>
      <input type="file" name="thumbnail" required><br><br>
   <div class="col-3" style="float:left;">
      <label>Nome do Shopping:</label><br>
      <input type="text" class="w75" name="shopping" placeholder="Nome Shopping" maxlength="125" required> <br><br>
      </div>
<div class="col-3" style="float:left;">
      <label>Cidade:</label><br>
       <input type="text" class="w75" name="cidade" placeholder="Cidade" maxlength="175" required> <br><br>
      </div>
<div class="col-3" style="float:left;">
      <label>Estado:</label><br>
       <input type="text" name="uf" placeholder="UF" maxlength="2" data-inputmask-regex="[A-Za-z]{2}-[\w]{2}-\d{2}" required > <br><br>
      </div>
      
<div class="col-6" style="float:left;">      
      <label>ABL:</label><br>
       <input type="text" name="abl" required> <br><br>
      </div>
      <div class="col-6" style="float:left;">
      <label>Visitas por mês:</label><br>
       <input type="text" name="visitas" placeholder="Números de visitas" required> <br><br>
      </div>
      
<div class="col-3" style="float:left;">      
      <label>Porcentagem de Classe A:</label><br>
       <input type="text" maxlength="5" name="a" required> <br><br>
      </div>
      <div class="col-3" style="float:left;">
      <label>Porcentagem de Classe B1:</label><br>
       <input type="text" maxlength="5" name="b1" required> <br><br>
      </div>
      <div class="col-3" style="float:left;">
      <label>Porcentagem de Classe B2:</label><br>
       <input type="text" maxlength="5" name="b2" required> <br><br>
      </div>
      
      <div class="col-3" style="float:left;">
      <label>Porcentagem de Classe C/D:</label><br>
       <input type="text" maxlength="5" name="cd" required> <br><br>
      </div>
      
      <div class="col-4" style="float:left;">
      <label>Midia Kit</label><br>
      <input type="file" name="midiakit" ><br><br>
      </div>
  </div>

       
</div>
     <button  type="submit" class="btn-admin-save">Salvar Alterações</button><br><br><br>
    
    </form>

<div class="card">
  <div class="card-header">
    <h3 class="card-title">Propriedades</h3>
                      <div class="card-tools">
      <!-- Collapse Button -->
      <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
    </div>

  </div>

  <div class="card-body">
      
    <div class="col-3" style="float:left;">
        <input type="checkbox">
      <label>Megabanner</label><br>
       <input type="file"> <br><br>
    </div>
      
      
      
    </div>

@endsection
