@extends('adminlte::page')

@section('title', 'Cobertura - midiaMalls Admin')

@section('content_header')
    <h1>Adicionar Nova Cobertura</h1>
@stop
@section('js')
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.ckeditor.com/4.11.4/standard/ckeditor.js"></script>

    <script>
        CKEDITOR.replace('class', {
            language: 'pt-br',
            customConfig: '/js/configmidia.js'
        });


        $(document).ready(function() {
            $('.dataTable').dataTable({
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.20/i18n/Portuguese-Brasil.json"
                }
            });
        });



        jQuery("input.percent")
            .mask("99%").blur(function() {
                var rg = /\,+\d/g; // regular expression for comma and number
                if (!rg.test(this.value)) { // check if value doesn't have any num after comma
                    this.value = this.value.split(',')[0]; // then just split the value and
                } // assign the index[0]
            });

        (function() {

            $("#UF").inputmask();

        })();


        @foreach ($propriedades as $shop)
        
        
            $(document).ready(function() {
            $('#check{{ $shop->id }}').change(function() {
            $('#checkshow{{ $shop->id }}').toggle();
            });
            });
        @endforeach

    </script>
@stop


@section('content')

    <form id="home" method="post" action="{{ URL::to('admin/nossos-shoppings/novo/inserir') }}"
        enctype="multipart/form-data">
        @method('PUT')
        {{ csrf_field() }}
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Novo Shopping</h3>
                <div class="card-tools">
                    <!-- Collapse Button -->
                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                            class="fas fa-minus"></i></button>
                </div>

            </div>

            <div class="card-body">

                <label>Imagem do Shopping <small>Seguir padrão de 400x334px</small></label><br>
                <input type="file" name="thumbnail" required accept="image/*"><br><br>
                <div class="col-3" style="float:left;">
                    <label>Nome do Shopping:</label><br>
                    <input type="text" class="w75" name="shopping"
                        onkeyup="this.value=this.value.replace(/[^a-zA-Z' 'áéíóúãõôâê]/g,'');" placeholder="Nome Shopping"
                        maxlength="125" required> <br><br>
                </div>
                <div class="col-3" style="float:left;">
                    <label>Cidade:</label><br>
                    <input type="text" class="w75" name="cidade"
                        onkeyup="this.value=this.value.replace(/[^a-zA-Z' 'áéíóúãõôâê]/g,'');" placeholder="Cidade"
                        maxlength="175" required> <br><br>
                </div>
                <div class="col-3" style="float:left;">
                    <label>Estado:</label><br>
                    <input type="text" name="uf" id="UF" placeholder="UF" maxlength="2" required> <br><br>
                </div>

                <!--<div class="col-6" style="float:left;">      
          <label>ABL:</label><br>
           <input type="text" name="abl" onkeyup="this.value=this.value.replace(/[^0-9'.'',']/g,'');" required> <br><br>
          </div>
        -->
                <div class="col-6" style="float:left;">
                    <label>Visitas por mês:</label><br>
                    <input type="text" name="visitas" onkeyup="this.value=this.value.replace(/[^0-9'.'',']/g,'');"
                        placeholder="Números de visitas" required> <br><br>
                </div>

                <!--<div class="col-3" style="float:left;">      
          <label>Porcentagem de Classe A:</label><br>
           <input type="text" class="percent" maxlength="5" name="a" required><br><small>Ex.: 01-99</small> <br><br>
          </div>
          <div class="col-3" style="float:left;">
          <label>Porcentagem de Classe B1:</label><br>
           <input type="text" class="percent" maxlength="5" name="b1" required><br><small>Ex.: 01-99</small> <br><br>
          </div>
          <div class="col-3" style="float:left;">
          <label>Porcentagem de Classe B2:</label><br>
           <input type="text" class="percent" maxlength="5" name="b2" required><br><small>Ex.: 01-99</small> <br><br>
          </div>
          
          <div class="col-3" style="float:left;">
          <label>Porcentagem de Classe C/D:</label><br>
           <input type="text" class="percent" maxlength="5" name="cd" required><br><small>Ex.: 01-99</small> <br><br>
          </div>
          
          <div class="col-4" style="float:left;">
          <label>Midia Kit</label><br>
          <input type="file" name="midiakit" ><br><br>
          </div>
            <div class="col-12" style="float:left;">
              <label>Destaque:</label>
                  <select name="destaque">
                  <option disabled>Destaque</option>
                      <option value="1" >Sim</option>
                      <option value="0" >Não</option>
                  </select>
     </div>-->

                <div class="col-12" style="float:left;">
                    <label>Classes:</label>
                    <br><textarea class="w100 class" name="class" id="class"
                        placeholder="Escreva aqui as classes"></textarea><br><br>
                </div>

                <div class="col-12" style="float:left;">
                    <label>URL do site:</label><br>
                    <input type="text" class="w100" name="site" placeholder="URL do site" maxlength="255"> <br><br>
                </div>



            </div>


        </div>


        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Propriedades</h3>
                <div class="card-tools">
                    <!-- Collapse Button -->
                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                            class="fas fa-minus"></i></button>
                </div>

            </div>


            <div class="card-body">
                <h3>Selecione as Propriedades que o Shoppping Possui</h3><br><br>


                @foreach ($propriedades as $prop)
                    <div class="col-3" style="float:left;">

                        <input type="checkbox" id="check{{ $prop->id }}" name="prop[]" value={{ $prop->id }}>
                        <label>{{ $prop->name }}</label><br>

                        <input type="hidden" name="prop_name[]" value="{{ $prop->name }}">
                        <input type="hidden" name="shopid[]" value="{{ $prop->id }}">

                    </div>
                @endforeach



            </div>


        </div>
        <button type="submit" class="btn-admin-save">Inserir Shopping</button><br><br><br>
    </form>

@endsection
