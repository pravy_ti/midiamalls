@extends('adminlte::page')

@section('title', 'Cobertura - midiaMalls Admin')

@section('content_header')
    <h1>Edição Cobertura</h1>
@stop

@section('css')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
@stop

@section('js')
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js"></script>

    <script src="https://cdn.ckeditor.com/4.11.4/standard/ckeditor.js"></script>

    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19//js/dataTables.bootstrap4.min.js"></script>
    
    <script>
        CKEDITOR.replace('content_text', {
            language: 'pt-br',
            customConfig: '/js/configmidia.js'
        });



        $(document).ready(function() {
            $('.dataTable').dataTable({
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.20/i18n/Portuguese-Brasil.json"
                }
            });

            $.fn.modal.Constructor.prototype._enforceFocus = function() {
                modal_this = this
                $(document).on('focusin.modal', function(e) {
                    if (modal_this.$element[0] !== e.target && !modal_this.$element.has(e.target)
                        .length &&
                        !$(e.target.parentNode).hasClass('cke_dialog_ui_input_select') &&
                        !$(e.target.parentNode).hasClass('cke_dialog_ui_input_text')) {
                        modal_this.$element.focus()
                    }
                })
            };
        });


        jQuery("input.percent")
            .mask("99%").blur(function() {
                var rg = /\,+\d/g; // regular expression for comma and number
                if (!rg.test(this.value)) { // check if value doesn't have any num after comma
                    this.value = this.value.split(',')[0]; // then just split the value and
                } // assign the index[0]
            });

        (function() {

            //$("#UF").inputmask();

        })();

    </script>
@stop


@section('content')
    <form id="home" method="post" action="{{ URL::to('admin/nossos-shoppings/update') }}" enctype="multipart/form-data">
        @method('PUT')
        {{ csrf_field() }}
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Banner</h3>
                <div class="card-tools">
                    <!-- Collapse Button -->
                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                            class="fas fa-minus"></i></button>
                </div>

            </div>

            <div class="card-body">


                <label>Banner Desktop</label>
                <br>
                <img src="{{ 'https://' . config('filesystems.disks.azure.name') . '.blob.core.windows.net/' . config('filesystems.disks.azure.container') }}/public/uploads/banners/{{ $banner->desktop }}" class="w100">
                <br><br>
                <input type="file" name="banner_desktop" accept="image/*">
                <br>
                <small>Imagem no formato PNG ou JPG, no formato de 1920x530px, com tamanho máximo de 2MB</small>
                <br><br>
                
                <label>Texto de Banner Desktop</label>
                <br>
                <input type="text" placeholder="Texto do Banner" class="banner_desk_title" maxlength="76"
                    name="banner_desk_title" value="{{ $banner->desktop_title }}"><br><small>Até 76 caracteres</small>
                <br><br>

                <label>Banner Mobile</label><br>
                <img src="{{ 'https://' . config('filesystems.disks.azure.name') . '.blob.core.windows.net/' . config('filesystems.disks.azure.container') }}/public/uploads/banners/{{ $banner->mobile }}">
                <br><br>
                <input type="file" name="banner_mobile" accept="image/*">
                <br>
                <small>Imagem no formato PNG ou JPG, no formato de 375x480px, com tamanho máximo de 2MB</small>
                <br><br>

                <label>Texto de Banner Mobile</label><br>
                <input type="text" placeholder="Texto do Banner" class="banner_mobile_title" maxlength="76"
                    name="banner_mobile_title" value="{{ $banner->mobile_title }}"><br><small>Até 76 caracteres</small>
                <br><br>
            </div>
        </div>

        <br>

        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Conteúdo</h3>
                <div class="card-tools">
                    <!-- Collapse Button -->
                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                            class="fas fa-minus"></i></button>
                </div>

            </div>

            <div class="card-body">
                <label>Titulo:</label><br> <input type="text" class="w50" maxlength="50" name="content_title"
                    value="{{ $page->title }}"><br><small>Até 50 Caracteres</small><br><br>
                <label>Texto:</label><br><textarea class="w50" maxlength="190"
                    name="content_text">{{ $page->text }}</textarea><br><small>Até 190 caracteres</small><br><br>

                <label>Titulo 2:</label><br> <input type="text" class="w50" maxlength="50" name="content_title2"
                    value="{{ $page->title2 }}"><br>
                <small>Até 50 Caracteres</small><br>
                <br>

                <label>Link externo do midiaKit nacional:</label><br> <input type="text" class="w100" maxlength="255"
                    name="midia_kit" value="{{ $page->midia_kit }}"><br><small>Link externo de nuvem (ex: one drive,
                    google
                    drive, etc)</small>
                    
                    <br><br>

                    <button type="submit" class="btn-admin-save float-left" form="home">Salvar Alterações</button>


                <?php
                /*<label>Midia Kit</label><br><br>
            @if ($page->image1 == null) @else
                    <a href="{{ 'https://' . config('filesystems.disks.azure.name') . '.blob.core.windows.net/' . config('filesystems.disks.azure.container') }}/public/uploads/files/{{ $page->image1 }}"
                        target="_blank"><i class="fa fa-file"> {{ $page->image1 }}</i></a> <br>

                @endif

                <input type="file" name="midiakit"><br><br>

            @if ($page->image1 == null) @else
                    <input type="checkbox" name="midiaglobal"><label>&nbsp; Deletar MidiaKit</label>
                    <!--
                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-sm-midiageral">

                              <i class="fa fa-window-close"> </i> Deletar MidiaKit
                            </button>-->
                @endif

                <br><br>

                <div class="modal fade" id="modal-sm-midiageral" style="display: none;" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">

                            <div class="modal-header">
                                <h4 class="modal-title">Deletar</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div class="modal-body">

                                <p>Tem certeza que deseja deletar o MidiaKit ?
                                </p>
                            </div>
                            <div class="modal-footer justify-content-between">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                                <a href="{{ URL::to('admin/nossos-shoppings/shopping/deletemidiageral') }}"><button
                                        type="submit" class="btn btn-danger" form="alo">Sim</button></a>
                            </div>

                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>

                <label>Tabela de Preços</label><br>

            @if ($page->image2 == null) @else
                    <a href="{{ 'https://' . config('filesystems.disks.azure.name') . '.blob.core.windows.net/' . config('filesystems.disks.azure.container') }}/public/uploads/files/{{ $page->image2 }}"
                        target="_blank"><i class="fa fa-file"> {{ $page->image2 }}</i></a> <br>

                @endif

                <input type="file" name="precos"><br><br>

            @if ($page->image2 == null) @else
                    <input type="checkbox" name="tabela"><label>&nbsp; Deletar Tabela de Preços</label>
                    <!-- <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-sm-preco">

                              <i class="fa fa-window-close"> </i> Deletar Tabela de Preços
                            </button>-->
                @endif

                <br><br>

                <div class="modal fade" id="modal-sm-preco" style="display: none;" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">

                            <div class="modal-header">
                                <h4 class="modal-title">Deletar</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <p>Tem certeza que deseja deletar a Tabela de Preços ?
                                </p>
                            </div>
                            <div class="modal-footer justify-content-between">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                                <a href="{{ URL::to('admin/nossos-shoppings/shopping/deletepreco') }}"><button
                                        type="submit" class="btn btn-danger" form="preco">Sim</button></a>
                            </div>

                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                */
                ?>




            </div>


        </div>

        
    </form>
    
    <br>

    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Shoppings</h3>
            <div class="card-tools">
                <!-- Collapse Button -->
                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
            </div>

        </div>


        <div class="card-body">
            <a href="nossos-shoppings/novo"> <button class="btn-info">Adicionar Shopping</button></a><br><br><br>

            <div id="example2_wrapper" class="dataTables_wrapper dt-bootstrap4">
                <div class="row">
                    <div class="col-sm-12 col-md-6"></div>
                    <div class="col-sm-12 col-md-6"></div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <table class="table table-bordered table-hover dataTable">
                            <thead>
                                <tr>
                                    <th>Shopping</th>
                                    <th>Estado</th>
                                    <th>Cidade</th>
                                    <th>Midia Kit</th>
                                    {{-- <th> Destaque</th> --}}
                                    <th>Ações</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($shoppings as $shopping)



                                    <tr>
                                        <td>{{ $shopping->name }}</td>
                                        <td>{{ $shopping->uf }}</td>
                                        <td>{{ $shopping->city }}</td>

                                        <td>
                                            @if ($shopping->midiakit == null)<i
                                                class="fa fa-file"></i> @else
                                                <a href="{{ url("storage/uploads/files/$shopping->midiakit") }}"
                                                    target="_blank"><i class="fa fa-file"></i></a>
                                            @endif
                                        </td>

                                        {{-- <td> <i class="fa fa-star" @if ($shopping->destaque == 1) style="color:#c8b83a" @else @endif></i>
                                        </td> --}}

                                        <td>
                                            <button type="button" class="btn btn-danger" data-toggle="modal"
                                                data-target="#modal-sm{{ $shopping->id }}">
                                                <i class="fa fa-window-close"></i>
                                            </button>
                                            <button type="button" class="btn btn-info" data-toggle="modal"
                                                data-target="#modal-lg{{ $shopping->id }}"><i
                                                    class="fa fa-pen"></i></button>
                                        </td>
                                    </tr>

                                    <div class="modal fade myModel" id="modal-lg{{ $shopping->id }}"
                                        style="display: none;" aria-hidden="true">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title">Editar {{ $shopping->name }} </h4>
                                                    <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                        <span aria-hidden="true">×</span>
                                                    </button>
                                                </div>
                                                <form id="home" method="post"
                                                    action="{{ URL::to('admin/nossos-shoppings/shopping/update') }}"
                                                    enctype="multipart/form-data">
                                                    @method('PUT')
                                                    {{ csrf_field() }}
                                                    <div class="modal-body">
                                                        <div class="card-body">


                                                            <label>Imagem do Shopping <small>Seguir padrão de 400x334px
                                                                    Formato PNG ou JPG</small></label><br>


                                                            <img src="{{ 'https://' . config('filesystems.disks.azure.name') . '.blob.core.windows.net/' . config('filesystems.disks.azure.container') }}/public/uploads/shoppings/{{ $shopping->image }}"
                                                                style="width:400px;height:334px">
                                                            <input type="file" name="thumbnail" accept="image/*"><br><br>
                                                            <div class="col-4" style="float:left;">
                                                                <label>Nome do Shopping:</label><br>
                                                                <input type="text" class="w100" name="shopping"
                                                                    placeholder="Nome Shopping" maxlength="125"
                                                                    onkeyup="this.value=this.value.replace(/[^a-zA-Z' 'áéíóúãõôâê]/g,'');"
                                                                    required value="{{ $shopping->name }}"> <br><br>
                                                            </div>
                                                            <div class="col-4" style="float:left;">
                                                                <label>Cidade:</label><br>
                                                                <input type="text" class="w100" name="cidade"
                                                                    placeholder="Cidade" maxlength="175" required
                                                                    onkeyup="this.value=this.value.replace(/[^a-zA-Z' 'áéíóúãõôâê]/g,'');"
                                                                    value="{{ $shopping->city }}"> <br><br>
                                                            </div>
                                                            <div class="col-3" style="float:left;">
                                                                <label>Estado:</label><br>
                                                                <input type="text" name="uf" placeholder="UF" id="UF"
                                                                    maxlength="2" required
                                                                    onkeyup="this.value=this.value.replace(/[^a-zA-Z]/g,'');"
                                                                    value="{{ $shopping->uf }}"> <br><br>
                                                            </div>

                                                            <!--<div class="col-6" style="float:left;">      
                      <label>ABL:</label><br>
                      <input type="text" name="abl" class="abl" required onkeyup="this.value=this.value.replace(/[^0-9'.'',']/g,'');" value="{{ $shopping->abl }}"> <br><br>
                      </div>
                -->
                                                            <div class="col-6" style="float:left;">
                                                                <label>Visitas por mês:</label><br>
                                                                <input type="text" name="visitas"
                                                                    placeholder="Números de visitas"
                                                                    onkeyup="this.value=this.value.replace(/[^0-9'.'',']/g,'');"
                                                                    required value="{{ $shopping->visits }}"> <br><br>
                                                            </div>

                                                            <!--<div class="col-3" style="float:left;">      
                  <label>Porcentagem de Classe A:</label><br>
                   <input type="text" maxlength="6" name="a" class="percent" required value="{{ $shopping->class_a }}"><br><small>Ex.: 01-99</small> <br><br>
                  </div>
                  <div class="col-3" style="float:left;">
                  <label>Porcentagem de Classe B1:</label><br>
                   <input type="text" maxlength="6" name="b1" class="percent" required value="{{ $shopping->class_b1 }}"><br><small>Ex.: 01-99</small> <br><br>
                  </div>
                  <div class="col-3" style="float:left;">
                  <label>Porcentagem de Classe B2:</label><br>
                   <input type="text" maxlength="6" name="b2" class="percent" required value="{{ $shopping->class_b2 }}"><br><small>Ex.: 01-99</small> <br><br>
                  </div>
                  
                  <div class="col-3" style="float:left;">
                  <label>Porcentagem de Classe C/D:</label><br>
                   <input type="text" maxlength="6" name="cd" class="percent" required value="{{ $shopping->class_cd }}"><br><small>Ex.: 01-99</small> <br><br>
                  </div>

                -->

                                                            <div class="col-12" style="float:left;">
                                                                <label>Classes:</label>
                                                                <br><textarea class="w100 class{{ $shopping->id }}"
                                                                    name="class" id="class{{ $shopping->id }}"
                                                                    placeholder="Escreva aqui as classes">{{ $shopping->class }}</textarea><br>
                                                            </div>
                                                            <script
                                                                src="https://cdn.ckeditor.com/4.11.4/standard/ckeditor.js">
                                                            </script>

                                                            <script>
                                                                CKEDITOR.replace('class' + "{{ $shopping->id }}", {
                                                                    language: 'pt-br',
                                                                    customConfig: '/js/configmidia.js'
                                                                });

                                                            </script>

                                                            <div class="col-12" style="float:left;">
                                                                <label>URL do site:</label><br>
                                                                <input type="text" class="w100" name="site"
                                                                    placeholder="URL do site" maxlength="255"
                                                                    value="{{ $shopping->site }}"> <br><br>
                                                            </div>




                                                            <div class="col-4" style="float:left;">
                                                                <label>Midia Kit</label><br>
                                                            @if (strlen($shopping->midiakit) > 3)
                                                                    <a href="{{ url("storage/uploads/files/$shopping->midiakit") }}"
                                                                        target="_blank"><i class="fa fa-file">
                                                                            {{ $shopping->midiakit }}</i></a>
                                                                @endif
                                                                <input type="file" name="midiakit">

                                                            @if (strlen($shopping->midiakit) > 3) <br><br>
                                                                    <button type="button" class="btn btn-danger"
                                                                        data-toggle="modal"
                                                                        data-target="#modal-sm-midia{{ $shopping->id }}">

                                                                        <i class="fa fa-window-close"> </i> Deletar MidiaKit
                                                                    </button>
                                                                @endif

                                                                {{-- <br><br>
                                                                <label>Destaque:</label>
                                                                <select name="destaque">
                                                                    <option disabled>Destaque</option>
                                                                    <option @if ($shopping->destaque == 1) selected @else @endif
                                                                        value="1">Sim</option>
                                                                    <option value="0" @if ($shopping->destaque == 0) selected @else @endif>Não
                                                                    </option>
                                                                </select> --}}
                                                            </div>

                                                            <input type="hidden" name="idshop"
                                                                value="{{ $shopping->id }}">
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer justify-content-between">
                                                        <div class="col-12" style="padding-left: 20px;">
                                                        <button type="submit" class="btn-admin-save float-left">Salvar
                                                            Alterações</button>
                                                        </div>

                                                    </div>
                                                </form>
                                            </div>
                                            <!-- /.modal-content -->
                                        </div>
                                        <!-- /.modal-dialog -->
                                        <div></div>
                                    </div>

                                    <div class="modal fade" id="modal-sm{{ $shopping->id }}" style="display: none;"
                                        aria-hidden="true">
                                        <div class="modal-dialog modal-sm">
                                            <div class="modal-content">
                                                <form id="home" method="post"
                                                    action="{{ URL::to('admin/nossos-shoppings/shopping/delete') }}"
                                                    enctype="multipart/form-data">
                                                    @method('PUT')
                                                    {{ csrf_field() }}
                                                    <div class="modal-header">
                                                        <h4 class="modal-title">Deletar</h4>
                                                        <button type="button" class="close" data-dismiss="modal"
                                                            aria-label="Close">
                                                            <span aria-hidden="true">×</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <p>Tem certeza que deseja deletar<br>
                                                            {{ $shopping->name }} ?
                                                        </p>
                                                        <input type="hidden" name="idshop" value="{{ $shopping->id }}">
                                                    </div>
                                                    <div class="modal-footer justify-content-between">
                                                        <button type="button" class="btn btn-default"
                                                            data-dismiss="modal">Fechar</button>
                                                        <button type="submit" class="btn btn-danger">Sim</button>
                                                    </div>
                                                </form>
                                            </div>
                                            <!-- /.modal-content -->
                                        </div>
                                        <!-- /.modal-dialog -->
                                    </div>


                                    <div class="modal fade" id="modal-sm-midia{{ $shopping->id }}" style="display: none;"
                                        aria-hidden="true">
                                        <div class="modal-dialog modal-sm">
                                            <div class="modal-content">
                                                <form id="home" method="post"
                                                    action="{{ URL::to('admin/nossos-shoppings/shopping/deletemidia') }}"
                                                    enctype="multipart/form-data">
                                                    @method('PUT')
                                                    {{ csrf_field() }}
                                                    <div class="modal-header">
                                                        <h4 class="modal-title">Deletar</h4>
                                                        <button type="button" class="close" data-dismiss="modal"
                                                            aria-label="Close">
                                                            <span aria-hidden="true">×</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <p>Tem certeza que deseja deletar o MidiaKit de
                                                            {{ $shopping->name }} ?
                                                        </p>
                                                        <input type="hidden" name="idshop" value="{{ $shopping->id }}">
                                                    </div>
                                                    <div class="modal-footer justify-content-between">
                                                        <button type="button" class="btn btn-default"
                                                            data-dismiss="modal">Fechar</button>
                                                        <button type="submit" class="btn btn-danger">Sim</button>
                                                    </div>
                                                </form>
                                            </div>
                                            <!-- /.modal-content -->
                                        </div>
                                        <!-- /.modal-dialog -->
                                    </div>




                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>Shopping</th>
                                    <th>Estado</th>
                                    <th>Cidade</th>
                                    <th>Midia Kit</th>
                                    {{-- <th> Destaque</th> --}}
                                    <th>Ações</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>

            </div>



        </div>


    </div>

@endsection
