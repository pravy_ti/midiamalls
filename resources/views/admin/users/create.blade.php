@extends('adminlte::page')

@section('title', 'Usuário')

@section('content_header')
    @if(isset($user->name)) <h1>Editar Usuário </h1>@else <h1>Novo Usuário</h1>@endif
@stop

@section('content')
    @if(count($errors) > 0)
        <div class="alert alert-danger">
            @foreach ($errors->all() as $message)
                {!! $message!!}<br />
            @endforeach
        </div>
    @endif
    @if (session('sucess'))
        <div class="alert alert-success">
            {{ session('sucess') }}
        </div>
    @endif
    {!! Form::open(array('route' => array('admin.users.store',isset($user->id) ? $user->id : ''),'id'=>'form-users','method' => 'POST','files'=>false)) !!}
        <div class="col-xs-6">
        <div class="form-group col-xs-12">
            <label for="">Nome</label>
            {!! Form::text('nome',isset($user->name) ? $user->name : '',array('class' => 'form-control','placeholder'=>'Nome usuário*','id'=>'nome') )!!}
        </div>
        <div class="form-group col-xs-12">
            <label for="">Email</label>
            {!! Form::text('email',isset($user->email) ? $user->email : '',array('class' => 'form-control','placeholder'=>'Email*','id'=>'email') )!!}
        </div>
        <div class="form-group col-xs-12">
            <label for="">Senha</label>
            {!! Form::password('senha',array('class' => 'form-control','placeholder'=>'Senha*') )!!}
            <small> Mínimo de 6 caracteres</small>
        </div>
<br>
            
    @if(Auth::user()->role == 55 || Auth::user()->role == 0)
        <label for="">Tipo de conta</label><br>
        <select name="role">
    @if(Auth::user()->role == 55)
        <option value="55" @if(isset($user->name)) @if($user->role == 55) selected @else @endif @endif>Master</option>   @endif
        <option value="0" @if(isset($user->name)) @if($user->role == 0) selected @else @endif @endif>Administrador</option>    
        <option value="1" @if(isset($user->name)) @if($user->role == 1) selected @else @endif @endif>Usuário</option>
        </select>
        
        <br><br>
@else @endif
            
     
        <div class="form-group col-xs-12">
            <button type="submit" class="btn btn-primary">Salvar</button>
            <a href="{{ url()->previous() }}" class="btn btn-default" >Cancelar</a>
        </div>
            </div>
    {!! Form::close() !!}
@stop



