@extends('adminlte::page')

@section('title', 'Lista de Usuários')

@section('css')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
@stop

@section('js')
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19//js/dataTables.bootstrap4.min.js"></script>
    
    <script>
        $(document).ready(function() {
            $('.dataTable').dataTable({
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.20/i18n/Portuguese-Brasil.json"
                }
            });
        });
    </script>
@stop


@section('content_header')
    <h1>Lista de Usuários</h1>
@stop


@section('content')
    <div class="box">

        @if(Auth::user()->role == 55 || Auth::user()->role == 0)
		<div class="box-header"> <a href="{{ route('admin.users.create') }}" class="btn btn-primary">Novo Usuário</a></div><br></br@else>@else @endif<!-- /.box-header -->
		@if (session('sucess'))
          <div class="alert alert-success">
              {{ session('sucess') }}
          </div>
      	@endif
		<div class="box-body">
			<table id="datatables" class="table table-bordered table-striped dataTable">
				<thead>
						
                        <th>Nome</th>
                        <th>E-mail</th>
@if(Auth::user()->role == 55 || Auth::user()->role == 0)
                    <th>Cargo</th>
                    @else @endif
                    
                        <th>Ações</th>
                    
				</thead>
				<tbody>
					@foreach($users as $user)
                    
                    @if(Auth::user()->role == 55)
						<tr>
							
							<td>{!! $user->name !!}</td>
							<td>{!! $user->email !!}</td>
                            
@if(Auth::user()->role == 55 || Auth::user()->role == 0)   <td>@if($user->role == 55) Master @elseif($user->role == 0) Administrador @elseif($user->role == 1) Usuário @endif </td>@else @endif
                            
                            
                            
                            <td>
				<a href="{{ route('admin.users.edit',$user->id) }}" class=""><button class="btn btn-info"><i class="fa fa-pen"></i></button></a>
                          <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-sm-midia{{$user->id}}">

                  <i class="fa fa-window-close"> </i>
                </button> 
							</td>
							
						</tr>
                    @endif
                    
                    
                    @if(Auth::user()->role == 1 )
                    <tr>
							
				    @if($user->role == 55 || $user->role == 0) @else
                        
                        
                            <td>{!! $user->name !!}</td>
							<td>{!! $user->email !!}</td>
                            <td>
				    @if(Auth::user()->id == $user->id)<a href="{{ route('admin.users.edit',$user->id) }}" class=""><button class="btn btn-info"><i class="fa fa-pen"></i></button></a>@else @endif
                
							</td>
							
						</tr>
                    
                    
                    @endif
                    @endif
                    
                    @if(Auth::user()->role == 0 )
                    
                    						<tr>
			         @if($user->id == Auth::user()->id )		
                                                
                    @if($user->role == 55) @else                        
							<td>{!! $user->name !!}</td>
							<td>{!! $user->email !!}</td>
                           
@if(Auth::user()->role == 55 || Auth::user()->role == 0)  <td> @if($user->role == 55) Master @elseif($user->role == 0) Administrador @elseif($user->role == 1) Usuário @endif @else </td> @endif
                                                
                            <td>
				<a href="{{ route('admin.users.edit',$user->id) }}" class=""><button class="btn btn-info"><i class="fa fa-pen"></i></button></a>

							</td>
							
						</tr>
                    
                    @endif
                    @else 
                    
                    @if($user->role == 55) @else 
                    				<tr>
							
							<td>{!! $user->name !!}</td>
							<td>{!! $user->email !!}</td>
                                        @if(Auth::user()->role == 55 || Auth::user()->role == 0)  <td> @if($user->role == 55) Master @elseif($user->role == 0) Administrador @elseif($user->role == 1) Usuário @endif @else </td> @endif
                            <td>
				<a href="{{ route('admin.users.edit',$user->id) }}" class=""><button class="btn btn-info"><i class="fa fa-pen"></i></button></a>
          <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-sm-midia{{$user->id}}">

                  <i class="fa fa-window-close"> </i>
                </button> 
							</td>
							
						</tr>
                    
                    @endif
                    @endif
                    @endif
                    
                    
                    
                    
                     <div class="modal fade" id="modal-sm-midia{{$user->id}}" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-sm">
          <div class="modal-content">
        <form id="home" method="post" action="{{ route('admin.users.destroy',$user->id) }}" enctype="multipart/form-data">
    @method('PUT')
  {{ csrf_field() }}
            <div class="modal-header">
              <h4 class="modal-title">Deletar</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <div class="modal-body">
              <p>Tem certeza que deseja deletar<br> 
                  {{$user->name}} ?
                </p>
                <input type="hidden" name="idshop" value="{{$user->id}}">
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
              <button type="submit" class="btn btn-danger">Sim</button>
            </div>
              </form>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>

                    
					@endforeach
				</tbody>
				<tfoot>
					<tr>
                        <th>Nome</th>
                        <th>E-mail</th>
                        @if(Auth::user()->role == 55 || Auth::user()->role == 0)   <th>Cargo</th>@else @endif
                        <th>Ações</th>
						
					</tr>
				</tfoot>
			</table>
      <br><br>
		</div><!-- /.box-body -->
<!-- /.box -->


   



@endsection