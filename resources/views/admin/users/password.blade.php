@extends('adminlte::page')

@section('title', 'Trocar Senha')

@section('content_header')
    <h1>Trocar Senha</h1>
@stop

@section('content')
    @if(count($errors) > 0)
        <div class="alert alert-danger">
            @foreach ($errors->all() as $message)
                {!! $message!!}<br />
            @endforeach
        </div>
    @endif
    @if (session('sucess'))
        <div class="alert alert-success">
            {{ session('sucess') }}
        </div>
    @endif
    {!! Form::open(array('route' => array('admin.users.storePassword',isset($user->id) ? $user->id : ''),'id'=>'form-users','method' => 'POST','files'=>false)) !!}
        
        <div class="form-group col-xs-12">
            <label for="">Senha atual</label>
            {!! Form::password('old_password',array('class' => 'form-control','placeholder'=>'Old Password*','id'=>'old') )!!}
        </div>
        <div class="form-group col-xs-12">
            <label for="">Nova Senha</label>
            {!! Form::password('password',array('class' => 'form-control','placeholder'=>'New Password*','id'=>'new') )!!}
        </div>
        <div class="form-group col-xs-12">
            <label for="">Confirmação nova senha</label>
            {!! Form::password('password_confirmation',array('class' => 'form-control','placeholder'=>'Confirmation Password*','id'=>'Confirmation') )!!}
        </div>

        <div class="form-group col-xs-12">
            <button type="submit" class="btn btn-primary">Salvar</button>
        </div>
    {!! Form::close() !!}
@stop



