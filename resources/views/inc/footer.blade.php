<footer id="footer" class="container-fluid -border-tlr -border-trr">
  <div class="container">
    <div class="row">
      <div class="col-12 col-lg-2">
        <img src="{{ 'https://' . config('filesystems.disks.azure.name'). '.blob.core.windows.net/' . config('filesystems.disks.azure.container') }}/public/uploads/icons/{{$footer->logo}}" alt="MidiaMalls" class="logo" style="width:140px;">
      </div>

      <div class="col-12 col-lg-3">
        <h4 class="h4 -white -fix-footer-spaces">Empresas afiliadas:</h4>

        <ul class="menu">
          <li><img src="{{ 'https://' . config('filesystems.disks.azure.name'). '.blob.core.windows.net/' . config('filesystems.disks.azure.container') }}/public/uploads/icons/{{$footer->company}}" alt="abooh" class="logo"></li>
          <li><img src="{{ 'https://' . config('filesystems.disks.azure.name'). '.blob.core.windows.net/' . config('filesystems.disks.azure.container') }}/public/uploads/icons/{{$footer->company2}}" alt="ivc" class="logo"></li>
        </ul>
      </div>

      <div class="col-12 col-lg-3">
        <h4 class="h4 -white -fix-footer-spaces">Siga nossos canais:</h4>

        @if($footer->linkedin !== NULL || $footer->instagram !== NULL || $footer->facebook !== NULL)
        <ul class="social-menu">
          @if($footer->facebook !== NULL)
            <li><a href="{{$footer->facebook}}" target="_blank"><i class="icon-facebook"></i></a></li>
          @endif
          @if($footer->linkedin !== NULL)
            <li><a href="{{$footer->linkedin}}" target="_blank"><i class="icon-linkedin"></i></a></li>
          @endif
          @if($footer->instagram !== NULL)
            <li><a href="{{$footer->instagram}}" target="_blank"><i class="icon-instagram"></i></a></li>
          @endif
        </ul>
        @endif
      </div>

      <div class="col-12 col-lg-4 d-flex flex-wrap">
        <div class="col-12 col-md-6 pl-0">
          <h4 class="h4 -white -fix-footer-spaces">Contato:</h4>
          <address>
             <a href="{{$footer->contact_link}}" target="_blank">{!! $footer->contact_adress !!}</a>
          </address>
        </div>

        <div class="col-12 col-md-6 p-0">
          <h4 class="h4 -hidden -fix-footer-spaces d-none d-md-block">Contato</h4>

          <address>
            @if($footer->phonenumber !== null)
              <p><a target="_blank" href="tel://{{$footer->phonenumber}}">{{$footer->phonenumber}}</a></p>
            @endif
            @if($footer->phonenumber2 !== null)
              <p><a target="_blank" href="tel://{{$footer->phonenumber2}}">{{$footer->phonenumber2}}</a></p>
            @endif
            @if($footer->phonenumber3 !== null)
              <p><a target="_blank" href="tel://{{$footer->phonenumber3}}">{{$footer->phonenumber3}}</a></p>
            @endif
            @if($footer->phonenumber4 !== null)
              <p><a target="_blank" href="tel://{{$footer->phonenumber4}}">{{$footer->phonenumber4}}</a></p>
            @endif
            @if($footer->phonenumber5 !== null)
              <p><a target="_blank" href="tel://{{$footer->phonenumber5}}">{{$footer->phonenumber5}}</a></p>
            @endif
          </address>
        </div>
      </div>
    </div>

    <div class="row copyright">
      @if($footer->terms !== null)
      <div class="col-12 col-lg-3">
          <p><a href="{{ 'https://' . config('filesystems.disks.azure.name'). '.blob.core.windows.net/' . config('filesystems.disks.azure.container') }}/public/uploads/files/{{$footer->terms}}" target="_blank" class="-white"><strong>Termos & Condicões de Uso</strong></a></p>
      </div>
      @endif

      @if($footer->policy !== null)
      <div class="col-12 col-lg-3 offset-lg-1">
        <p><a href="{{ 'https://' . config('filesystems.disks.azure.name'). '.blob.core.windows.net/' . config('filesystems.disks.azure.container') }}/public/uploads/files/{{$footer->policy}}" target="_blank" class="-white"><strong>Política de Privacidade</strong></a></p>
      </div>
      @endif

      <div class="col-12 col-lg-4 signature">
        <p class="-small">midiaMalls© @php echo date("Y"); @endphp - Todos os direitos reservados  </p>
      </div>
    </div>
  </div>
</footer>
<script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.js"></script>


<script type="text/javascript" src="//cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick.min.js"></script>
<script src="/js/functions.js"></script>


</body>

</html>
