<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

    @if( isset($seo_page_title) )
        <title>{{ $seo_page_title }}</title>
    @else
        <title>MidiaMalls</title>
    @endif

    @if( isset($seo_description) )
        <meta name="description" content="{{ $seo_description }}">
    @endif

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Fira+Sans&display=swap" rel="stylesheet">

        <!-- Styles -->
        <link href="/css/base.css" rel="stylesheet" type="text/css" >
        <link href="/css/app.css" rel="stylesheet" type="text/css" >

        <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick.css"/>
        <!-- Add the slick-theme.css if you want default styling -->

        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=G-M4Q8NQZXPY"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());
            gtag('config', 'G-M4Q8NQZXPY');
        </script>
    </head>

    <body>
        <header id="header" class="container-fluid">
            <div class="container">
                <div class="d-flex holder-header">
                    <div class="logo">
                        <a href="/"><img src="/images/logo_color.png" class="logo-b"></a>
                    </div>

                    <div class="holder-hamburger d-flex">
                        <div id="btn-menu" class="hamburger">
                            <span class="line"></span>
                            <span class="line"></span>
                            <span class="line"></span>
                        </div>
                        <p class="text-menu">Menu</p>
                    </div>

                </div>
            </div>

            <div class="nav-menu container-fluid">
                <div class="container">
                    <nav class="d-flex justify-content-between w-100">
                        <div class="d-flex col-12 p-0">
                            <div class="nav-wrap">
                                <ul class="group d-flex flex-column" id="titles">
                                    <li>
                                      <a href="/">Home</a>
                                    </li>
                                    <!-- <li >
                                      <a href="/midiamalls">mídiaMALLS</a>
                                    </li> -->
                                    <li >
                                      <a href="/nossos-shoppings">Nossos Shoppings</a>
                                    </li>
                                    <!-- <li >
                                      <a href="/propriedades">Propriedades</a>
                                    </li> -->
                                    <!-- <li id="contact_footer">
                                      <a href="#footer">Contato</a>
                                    </li> -->
                                </ul>
                            </div>
                        </div>
                    </nav>
                </div>
            </div>
        </header>
