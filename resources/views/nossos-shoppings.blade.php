@include('inc.header', ['seo_page_title' => $SEO_vars['page_title'], 'seo_description' => $SEO_vars['description']])

{{--

Midia Kit: $page->midia_kit

Classes
$shop->class

--}}

<div id="inicio"></div>

<div class="banner_desktop interna" style="background-image: url({{ 'https://' . config('filesystems.disks.azure.name'). '.blob.core.windows.net/' . config('filesystems.disks.azure.container') }}/public/uploads/banners/{{$banner->desktop}})">
    <div class="container d-flex align-content-center justify-content-center align-items-center">
        <div class="col-8 -center">
            <h1 class="h1 -white align-center">
                {{$banner->desktop_title}}
            </h1>
        </div>
    </div>
</div>

<div class="banner_mobile interna" style="background-image:url({{ 'https://' . config('filesystems.disks.azure.name'). '.blob.core.windows.net/' . config('filesystems.disks.azure.container') }}/public/uploads/banners/{{$banner->mobile}})">
    <div class="container d-flex align-content-center justify-content-center align-items-center">
        <div class="col-8 -center">
            <h1 class="h1 -white align-center">
                {{$banner->mobile_title}}
            </h1>
        </div>
    </div>
</div>

<section id="malls_inner" class="container-fluid -m-top-0">
  <div class="container">
    <div class="row">
      <div class="col-12 col-lg-8">
        <h2 class="title h2 -orange view">{{$page->title}}</h2>
        <p class="m-0 view" style="font-size:26px;">{!! $page->text !!}</p>
        <h2 class="title h2 -orange view">{{$page->title2}}</h2>

        @if($page->midia_kit  !== null)
          <p><a href="{{$page->midia_kit }}" class="link download -nblue" target="_blank">BAIXAR MÍDIA KIT NACIONAL <i class="icon-next"></i></a></p>
        @endif
      </div>

      <div class="col-12 col-lg-10 offset-lg-1 -m-top-small -m-bottom-small">
        <div class="row">
          <div class="cards-holder -white  d-flex justify-content-start flex-wrap">
            @foreach($shoppings as $shop)
              <div data-region="sudeste" data-uf="SP" data-city="São Paulo" class="card -border-brr -m-item">
                <div class="holder-content">
                  <div class="image-holder" style="background-image: url({{ 'https://' . config('filesystems.disks.azure.name'). '.blob.core.windows.net/' . config('filesystems.disks.azure.container') }}/public/uploads/shoppings/{{$shop->image}});"></div>
                </div>

                <div class="holder-content">
                  <!-- <pre>@php var_dump($shop) @endphp</pre> -->
                  <h2 class="h5 -nblue">{{$shop->name}}</h2>
                  <p>{{$shop->city}} - {{$shop->uf}}</p>
                  @if($shop->visits)
                    <p>
                      {{$shop->visits}} visitas/mês.
                    </p>
                  @endif

                  @if($shop->class)
                    {!! $shop->class !!}
                  @endif

                  @if($shop->site !== null)
                    <p>
                      <a href="{{$shop->site}}" class="link -nblue -small view" target="_blank">SITE DO SHOPPING <i class="icon-next"></i></a>
                    </p>
                  @endif
                  @if($shop->midiakit !== null)
                    <p>
                      <a href="{{ 'https://' . config('filesystems.disks.azure.name'). '.blob.core.windows.net/' . config('filesystems.disks.azure.container') }}/public/uploads/files/{{$shop->midiakit}}" class="link -nblue -small view" target="_blank">MÍDIA KIT <i class="icon-next"></i></a>
                    </p>
                  @endif
                </div>
              </div>
            @endforeach
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<style>
.card a:after {
    background-color: #f37021!important;
}
#malls .download:after {
    background-color: #0097A5!important;
}

section  ul {
  margin-left: 3em!important;
  margin-bottom:35px;
  list-style:none;
}
section  ol {
  margin-left: 3.5em!important;
  margin-bottom:0px;
}

section ul li::before{
  background-color:#f37020;
}

section ul li::before {
  content: '';
  display: inline-table;
  vertical-align: middle;
  width: 4px;
  height: 21px;
  background-color: #f37020;
  border-bottom-left-radius: 25px;
  border-top-right-radius: 25px;
  -webkit-transform: rotateZ(30deg);
  transform: rotateZ(30deg);
  margin-right: 15px;
  margin-left: 0px;
  top: -.2rem;
  position: relative;
}

section li {
  margin-bottom:0px;
}
</style>

@include('inc.footer')
