@include('inc.header')

<div id="inicio"></div>
<div class="banner_desktop interna" style="background-image:url({{ 'https://' . config('filesystems.disks.azure.name'). '.blob.core.windows.net/' . config('filesystems.disks.azure.container') }}/public/uploads/banners/{{$banner->desktop}})">
  <div class="container d-flex align-content-center justify-content-center align-items-center">
    <div class="col-8 -center">
      <h1 class="h1 -white align-center">{{$banner->desktop_title}}</h1>
    </div>
  </div>
</div>

<div class="banner_mobile interna" style="background-image:url({{ 'https://' . config('filesystems.disks.azure.name'). '.blob.core.windows.net/' . config('filesystems.disks.azure.container') }}/public/uploads/banners/{{$banner->mobile}})">
  <div class="container d-flex align-content-center justify-content-center align-items-center">
    <div class="col-8 -center">
      <h1 class="h1 -white align-center">{{$banner->mobile_title}}</h1>
    </div>
  </div>
</div>

<div class="col-9 offset-2">
  <div class="element-div4">
    <img class="element-logo4" src="images/element.png">
  </div>
</div>

<section id="propriedades" class="container-fluid m-top-0">
    <div class="container">
        <div class="row">

            <div class="d-none d-lg-block col-1"></div>

            <div class="col-10 -m-top-small">

                <h2 class="h5 -black -dot-black">Selecione uma propriedade abaixo:</h2>

            </div>

            <div class="d-none d-lg-block col-1"></div>
            <div class="d-none d-lg-block col-1"></div>


           <div class="col-12 col-lg-3 -m-mobile-item">
             <!--   <div class="line">
                    <div class="full">
                        <select name="propriedade" placeholder="Propriedades" class="-nblue all" model="form.fields.propriedades">
                            <option :value="null" selected>Propriedades</option>


                        </select>

                    </div>
                </div> -->
            </div>
                <!-- <div class="col-12 col-lg-2 pl-lg-0 -m-mobile-item">
           <div class="line">
                    <div class="full">
                        <select name="region" id="" placeholder="Região" class="-nblue" v-if='locale != "en"'>
                            <option :value="null" selected>Região</option>
                            <option value="centro-oeste">Centro-Oeste</option>
                            <option value="norte">Norte</option>
                            <option value="nordeste">Nordeste</option>
                            <option value="sudeste">Sudeste</option>
                            <option value="sul">Sul</option>
                        </select>

                    </div>
                </div>
            </div>-->
            <!-- <div class="col-12 col-lg-2 -m-mobile-item">
               <div class="line">
                    <div class="full stateSelect">
                        <select name="state" placeholder="Estado" class="-nblue all">
                            <option :value="null" selected>Estado</option>
                            <option></option>
                        </select>

                    </div>
                </div>
            </div>-->
             <!-- <div class="col-12 col-lg-3 pr-lg-0">
              <div class="line">
                    <div class="full">
                        <select class="-nblue" name="city" placeholder="Cidade" disabled v-if='locale != "en"'>
                            <option :value="null" selected>Cidade</option>
                        </select>
                    </div>
                </div>
            </div> -->

            <div class="col-12 col-lg-10 offset-lg-1 -m-top-small -m-bottom-last-small -p-big-lg">

                @foreach($propriedades as $prop)
                <div class="holder-tabs">
                    <div class="tab-link col-12" id="{{$prop->id}}">
                        <h5 class="h4 -orange -m-bottom-small">{{$prop->name}}</h5>

                    </div>

                    <div class="row list-shoppings -m-item tab-content" id="{{$prop->id}}" style="display: none;">
                        <div class="cards-holder -white -carousel d-flex justify-content-start flex-wrap">


                            @foreach($shoppings as $shop)

                            @foreach($shopro as $pro)

                            @if($pro->id == $shop->id)
                            @if($pro->name == $prop->name)

                           <div class="card -m-item -border-brr">

                            <div class="image-holder" style="background-image: url({{ 'https://' . config('filesystems.disks.azure.name'). '.blob.core.windows.net/' . config('filesystems.disks.azure.container') }}/public/uploads/shoppings/{{$shop->image}});"></div>

                                <div class="holder-content">
                                    <h2 class="h5 -nblue">{{$shop->name}}</h2>


                                    <p>{{$shop->city}} - {{$shop->uf}}</p>

                                    <p>ABL: {{$shop->abl}} m²<br />
                                        {{$shop->visits}} visitas/mês.<br>
                                        Classe A:{{$shop->class_a}}<br>
                                    Classe B1: {{$shop->class_b1}}<br>
                                    Classe B2: {{$shop->class_b2}}<br>
                                    Classe C/D: {{$shop->class_cd}}<br></p>



                                </div>
                                </div>
                            @else @endif
      @else @endif
                            @endforeach
                                     @endforeach


                        </div>
                        </div>
                </div>


            @endforeach






            </div>
        </div>
</section>

<style>
  section  ul {
        margin-left: 3em!important;
        margin-bottom:35px;
        list-style:none;
    }
      section  ol {
        margin-left: 3.5em!important;
        margin-bottom:35px;
    }

   section ul li::before{background-color:#f37020;}

section ul li::before {content: '';
display: inline-table;
vertical-align: middle;
width: 4px;
height: 21px;
background-color: #f37020;
border-bottom-left-radius: 25px;
border-top-right-radius: 25px;
-webkit-transform: rotateZ(30deg);
transform: rotateZ(30deg);
margin-right: 15px;
margin-left: 0px;
top: -.2rem;
position: relative;
    }


</style>


@include('inc.footer')
