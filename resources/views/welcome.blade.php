@include('inc.header', ['seo_page_title' => $SEO_vars['page_title'], 'seo_description' => $SEO_vars['description']]);

<div id="inicio"></div>

<div class="banner_desktop" style="background-image: url({{ 'https://' . config('filesystems.disks.azure.name'). '.blob.core.windows.net/' . config('filesystems.disks.azure.container') }}/public/uploads/banners/{{$banner->desktop}})">
    <div class="container d-flex align-content-center justify-content-center align-items-center">
        <div class="col-8 -center">
            <h1 class="h1 -white align-center">
                {{$banner->desktop_title}}
            </h1>
        </div>
    </div>
</div>

<div class="banner_mobile" style="background-image: url({{ 'https://' . config('filesystems.disks.azure.name'). '.blob.core.windows.net/' . config('filesystems.disks.azure.container') }}/public/uploads/banners/{{$banner->mobile}})">
    <div class="container d-flex align-content-center justify-content-center align-items-center">
        <div class="col-10 -center">
            <h1 class="h1 -white align-center">
                {{$banner->mobile_title}}
            </h1>
        </div>
    </div>
</div>

<div>
    <div class="fixed-menu">
        <div class="container">
            <ul class="col-2">
                <li><a href="#inicio">INÍCIO</a></li>
                <li><a href="#midia">Sobre nós</a></li>
                <li><a href="#anunciar">Por que Anunciar</a></li>
                <li><a href="#malls">Cobertura</a></li>
                <li><a href="#propriedades">Formatos disponíveis</a></li>
                <li><a href="#midiakit">Midia kit</a></li>
                <li><a href="#contact">Vamos conversar</a></li>
                <div class="line">
                  <div class="menu-stopper"></div>
                </div>
            </ul>
        </div>
    </div>
    <div class="menu-free"></div>

    <section id="midia" class="container-fluid">
        <div class="container">
            <div class="row d-flex justify-content-end align-items-center">
                <div class="col-md-5 text-holder">
                    <h2 class="h3 title -dot-black -black"><a href="/{{$page->title_link}}">{{$page->title}}</a></h2>

                    <h3 class="-orange mb-5">{!! $page->text !!}</h3>
                </div>

                <div class="col-md-5 image-holder">
                    <img src="{{ 'https://' . config('filesystems.disks.azure.name'). '.blob.core.windows.net/' . config('filesystems.disks.azure.container') }}/public/uploads/pages/{{$page->image1}}" class="w-100">
                </div>
            </div>

            <div class="row d-flex justify-content-end align-items-center">
                <div class="col-md-5 image-holder">
                    <img src="{{ 'https://' . config('filesystems.disks.azure.name'). '.blob.core.windows.net/' . config('filesystems.disks.azure.container') }}/public/uploads/pages/{{$page->image2}}" class="w-100">
                </div>
                <div class="col-md-5 text-holder">
                    <h2 class="-black">{{$page->title2}}</h2>

                    <h3 class="-orange">{!! $page->text2 !!}</h3>
                </div>
            </div>
        </div>
    </section>

    <section id="anunciar" class="container-fluid -border-tlr -border-brr -m-top-big -p-top-small">
        <div class="container d-flex justify-content-end">
            <div class="row col-xl-10 p-0 m-0">
                <h2 class="h3 -black title -dot-black view">{{$page->title3}}</h2>
            </div>
        </div>

        <div class="container d-flex justify-content-end">
            <div class="row col-xl-10 p-0 m-0">
                <h2 class="h3 -white title">{{$page->text3}}</h2>
            </div>
        </div>

        <div class="container d-flex justify-content-end">
          <div class="row d-flex justify-content-end">
            <div class="col-12 col-xl-10">
              <div class="row -m-item-collapse" style="margin-bottom:0px!important;">
                  @foreach($announce as $ann)
                    @if($ann->title !== null)
                    <div class="col-12 col-lg-4 -m-item mt-4">
                        <p class="mb-2 view">
                          <img  style="width:80px;height:80px" src="{{ 'https://' . config('filesystems.disks.azure.name'). '.blob.core.windows.net/' . config('filesystems.disks.azure.container') }}/public/uploads/icons/{{$ann->icon}}" alt="Presença em todas as regiões do Brasil" class="ico">
                        </p>

                        <h3 class="h5 -white view">{{$ann->title}}</h3>
                    </div>
                    @endif
                  @endforeach
              </div>
            </div>

            @if($page->midia_kit_completo !== null)
              <div class="col-12 col-xl-10 -m-bottom-small">
                <p><a href="{{$page->midia_kit_completo}}" target="_blank" class="link -white">
                  Saiba mais <i class="icon-next"></i>
                </a></p>
              </div>
            @endif
          </div>
        </div>
    </section>

    <section id="malls" class="container-fluid -m-top-big">
      <div class="container">
        <div class="row">
          <div class="col-12 col-md-5 col-xl-4 offset-xl-2">
            <h2 class="h3 -black title -dot-black">{{$page->title4}}</h2>

            @if($page->title_link4 !== null)
              <h2 class="h3 title -orange -m-bottom-0">{{$page->title_link4}}</h2>
            @endif

            @if($page->text4 && $page->text4 !== '')
              {!! $page->text4 !!}
            @endif

            <a href="/nossos-shoppings" class="link -nblue">
              Conheça nossos shoppings <i class="icon-next"></i>
            </a>
          </div>

          <div class="col-sm-12 offset-sm-0  col-md-6 offset-md-1  col-xl-5 pl-xl-1">
            <img src="{{ 'https://' . config('filesystems.disks.azure.name'). '.blob.core.windows.net/' . config('filesystems.disks.azure.container') }}/public/uploads/pages/{{$page->image3}}" class="w-100" />
          </div>
        </div>
      </div>
    </section>

    <section id="propriedades" class="w-100 -border-tlr -m-top-big -p-bottom-last-small">
      <div class="container">
        <div class="row p-0 m-0 d-flex justify-content-end">
          <div class="col-12 col-xl-10">
            <h2 class="h3 title -black -dot-black">{{$page->title5}}</h2>

            @if($page->title_link5 !== null)
              <h2 class="h3 title -orange -m-bottom-0">{{$page->title_link5}}</h2>
            @endif

            @if($page->text5  !== null)
              {!! $page->text5 !!}
            @endif
          </div>
        </div>
      </div>

      @php $resultProp = json_decode($propriedades, true) @endphp
      <div id="carousel" class="formatos-image -m-top-small">
        <img src="{{ 'https://' . config('filesystems.disks.azure.name'). '.blob.core.windows.net/' . config('filesystems.disks.azure.container') }}/public/uploads/propriedades/{{$resultProp[0]['image']}}" alt="megabanner" id="prop_banner" class="image-position w-100" style="max-width:100%;max-height:550px;">
      </div>
      <div class="container mt-5">
        <div class="row">
          <div class="col-12 col-xl-8 offset-xl-2 d-flex flex-wrap">
            @php $i=0; @endphp
            @foreach($resultProp as $prop) @php $i++; @endphp
              <div class="formatos-seletor col-6 col-md-2 text-center -cursor -m-item-min @php if( $i == 1 ) { echo 'active'; } @endphp"
                data-image="{{ 'https://' . config('filesystems.disks.azure.name'). '.blob.core.windows.net/' . config('filesystems.disks.azure.container') }}/public/uploads/propriedades/{{$prop['image']}}"
                data-icon="{{ 'https://' . config('filesystems.disks.azure.name'). '.blob.core.windows.net/' . config('filesystems.disks.azure.container') }}/public/uploads/icons/{{$prop['icon']}}"
                data-hover="{{ 'https://' . config('filesystems.disks.azure.name'). '.blob.core.windows.net/' . config('filesystems.disks.azure.container') }}/public/uploads/icons/{{$prop['icon_hover']}}"
                >
                <p class="m-0">
                  @if( $i == 1 )
                    <img src="{{ 'https://' . config('filesystems.disks.azure.name'). '.blob.core.windows.net/' . config('filesystems.disks.azure.container') }}/public/uploads/icons/{{$prop['icon_hover']}}" class="icon-default -max-100">
                  @else
                    <img src="{{ 'https://' . config('filesystems.disks.azure.name'). '.blob.core.windows.net/' . config('filesystems.disks.azure.container') }}/public/uploads/icons/{{$prop['icon']}}" class="icon-default -max-100">
                  @endif
                </p>
                <p class="@php if( $i == 1 ) { echo '-orange'; } else { echo '-nblue'; } @endphp  m-0">{{$prop['name']}}</p>
              </div>
            @endforeach
          </div>
        </div>
      </div>
    </section>

    <section id="midiakit" class="container-fluid -m-top-0">
      <div class="container">
        <div class="row">
          <div class="col-12 col-md-5 col-xl-4 offset-xl-2">
            <h2 class="h3 -black title -dot-black">{{$page->title6}}</h2>

            <h2 class="h3 title -orange -m-bottom-0">{{ $page->subtitle6 }}</h2>

            {!! $page->text6 !!}

            @if($page->midia_kit  !== null)
              <p><a href="{{$page->midia_kit}}" target="_blank" class="link -nblue">
                BAIXAR MÍDIA KIT NACIONAL <i class="icon-next"></i>
              </a></p>
            @endif

            @if(isset($page->title_link6)  !== null)
              <p><a href="{{ 'https://' . config('filesystems.disks.azure.name'). '.blob.core.windows.net/' . config('filesystems.disks.azure.container') }}/public/uploads/pages/{{$page->title_link6}}" target="_blank" class="link -nblue">
                BAIXAR TABELA DE PREÇOS <i class="icon-next"></i>
              </a></p>
            @endif
          </div>

          <div class="col-sm-12 offset-sm-0  col-md-6 offset-md-1  col-xl-5 pl-xl-1">
            @if(isset($page->image4) !== null)
              <img src="{{ 'https://' . config('filesystems.disks.azure.name'). '.blob.core.windows.net/' . config('filesystems.disks.azure.container') }}/public/uploads/pages/{{$page->image4}}" class="w-100" />
            @else
              <img src="/images/midia-kit.jpg" class="w-100" />
            @endif
          </div>
        </div>
      </div>
    </section>

    <section id="contact" class="container-fluid -p-top-small -m-top-big bg-fixed" style="background-color:#333;background-image: url({{ 'https://' . config('filesystems.disks.azure.name'). '.blob.core.windows.net/' . config('filesystems.disks.azure.container') }}/public/uploads/banners/{{$banner_con->desktop}})">
      <div class="container">
        <div class="row">
          <div class="col-12 col-md-6 col-xl-5 pr-lg-0 offset-xl-2">
            <h2 class="h3 title -white -dot">Vamos conversar</h2>

            <!-- <form id="fale" name="form" action="/fale-conosco" onsubmit="checkForm()" ref="contactForm"> -->
            <form id="fale" name="form" onsubmit="checkForm()" action="/fale-conosco" ref="contactForm">
              @method('PUT')
              {{ csrf_field() }}
              <div class="line">
                <div class="full form-field">
                  <label for="name" class="-white">Nome completo</label>
                  <input type="text" name="name" id="name" class="required -white" />
                </div>
              </div>

              <div class="line">
                <div class="full form-field">
                  <label for="email" class="-white">E-mail</label>
                  <input type="email" name="email" id="email" class="required  -white" />
                </div>
              </div>

              <div class="line">
                <div class="full form-field">
                  <label for="message" class="-white" >Mensagem</label>
                  <textarea name="message" id="message" class="required  -white"></textarea>
                </div>
              </div>

              <div class="line">
                <div class="full">
                  <button type="submit" form="fale" class="link -white">Enviar <i class="icon-next"></i></button>
                </div>
              </div>
            </form>
          </div>

          <div class="col-12 col-md-5 col-xl-4">
            <div class="infos-box -border-tlr -border-brr min-margin">

            <p class="-white">
              <strong>Nosso e-mail:</strong><br />
              <a href="mailto:{{$contact->title}}">{{$contact->title}}</a>
            </p>

            <p class="-white">
              <strong>Contato:</strong><br />
              <a href="tel://{{$contact->text}}">{{$contact->text}}</a>
            </p>

            <p class="-white">
              <strong>Nosso escritório:</strong><br>
              <a class="contato-link" href="{{$contact->title2}}" target="_blank">{!!$contact->text2!!}</a>
            </p>
          </div>
        </div>

        <div class="stop-fixed"></div>
      </div>
    </div>
  </section>

</div>

<div class="stop-fixed"></div>

<style>
    #midia {margin-bottom:110px!important;}
</style>

<script src="https://code.jquery.com/jquery-1.12.4.js"></script>

<script>
$('.formatos-seletor').click(function() {
  let iconDefault = $('.formatos-seletor.active').data('icon');
  $('.formatos-seletor.active').find('img').attr('src', iconDefault);
  $('.formatos-seletor.active').find('p').addClass('-nblue').removeClass('-orange');
  $('.formatos-seletor.active').removeClass('active');

  let imageBanner = $(this).data('image');
  let iconHover = $(this).data('hover');
  $(this).addClass('active');
  $(this).find('img').attr('src', iconHover);
  $(this).find('p').addClass('-orange').removeClass('-nblue');
  $('#prop_banner').attr('src', imageBanner);

  $("html, body").animate({ scrollTop: $('#carousel').offset().top - 0 }, 500);
});

//Formulário

function data() {
  if (localStorage.locale && localStorage.locale.toLowerCase() == "en"){
    return {
      url: $('form#fale').attr('action'),
      form: {
          isValidated: false,
          showErrors: 0,
          validation: {
              name: true,
              mail: true,
              message: true
          },
          messages: {
              name: 'Fill your name correctly',
              mail: 'Fill your e-mail correctly',
              message: 'Mandatory field',
          },
          fields: {
              name: null,
              mail: null,
              message: null
          }
      }
    }
  }else{
    return {
      url: $('form#fale').attr('action'),
      form: {
        isValidated: false,
        showErrors: 0,
        validation: {
            name: true,
            mail: true,
            message: true
        },
        messages: {
            name: 'Digite seu nome corretamente',
            mail: 'Inisira seu e-mail corretamente',
            message: 'Campo obrigatório',
        },
        fields: {
            name: null,
            mail: null,
            message: null
        }
      }
    }
  }
};

function checkForm(e) {
  e.preventDefault();
  console.log('here');

  this.form.isValidated = true;
  this.form.showErrors = 0;

  //NAME VALIDATION
  if ( !this.form.fields.name || (this.form.fields.name && this.form.fields.name.length == 0) ) {
      this.form.showErrors = 1;
      this.form.validation.name = false;
      this.form.isValidated = false;
  } else {
    this.form.validation.name = true;
  }

  //MAIL VALIDATION
  var validMail = false;

  if ( this.form.fields.mail || (this.form.fields.mail && this.form.fields.mail.length > 0) ) {
    var mailRegex = new RegExp("[a-zA-Z0-9\-.]*@{1}[a-zA-Z0-9\-]+.[a-zA-Z0-9.]+$");
    validMail = mailRegex.test(this.form.fields.mail);
  }

  if ( !this.form.fields.mail || (this.form.fields.mail && this.form.fields.mail.length == 0) || !validMail) {
      this.form.showErrors = 1;
      this.form.validation.mail = false;
      this.form.isValidated = false;
  } else {
    this.form.validation.mail = true;
  }

  //MESSAGE VALIDATION
  if ( !this.form.fields.message || (this.form.fields.message && this.form.fields.message.length == 0) ) {
      this.form.showErrors = 1;
      this.form.validation.message = false;
      this.form.isValidated = false;
  } else {
    this.form.validation.message = true;
  }

  if ( !this.form.isValidated ){
    window.setTimeout(() => {
      this.form.showErrors = false;
    }, 3000);
  } else {
    $('form#fale').append('<p class="-gray4 -small m-0 response-form">Enviando dados, aguarde...</p>');

    $.ajax({
      type: "POST",
      url: $('form#fale').attr('action'),
      data: this.form.fields,
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      success: function (result) {
        $('.response-form').remove();
        $('form#fale').append('<p class="-white -small m-0 response-form">Formulário enviado com sucesso!</p>');
        setTimeout(function(){
            $('.response-form').remove();
        }, 2000);
        //window.location.reload();
      },
      error: function (xhr, ajaxOptions, thrownError) {
        $('.response-form').remove();
        $('form#fale').append('<p class="-white -small m-0 response-form view">Ocorreu um erro ao enviar sua mensagem.</p>');
        setTimeout(function(){
            $('.response-form').remove();
        }, 2000);
      }
    });
  }
};

$('form#fale').submit(function(e){
  e.preventDefault();

  let hasErrors = false;

  if( $('input[name="name"]') === '' || $('input[name="name"]').val().length < 3 ) {
    $('input[name="name"]').after('<p class="-white -small m-0 validation-form">Este campo é obrigatório</p>');
    hasErrors = true;
  }

  var mailRegex = new RegExp("[a-zA-Z0-9\-.]*@{1}[a-zA-Z0-9\-]+.[a-zA-Z0-9.]+$");
  if( !mailRegex.test($('input[name="email"]').val()) ) {
    $('input[name="email"]').after('<p class="-white -small m-0 validation-form">Este campo é obrigatório</p>');
    hasErrors = true;
  }

  if( $('textarea[name="message"]') === '' || $('textarea[name="message"]').val().length < 3 ) {
    $('textarea[name="message"]').after('<p class="-white -small m-0 validation-form">Este campo é obrigatório</p>');
    hasErrors = true;
  }

  if( hasErrors ) {
    setTimeout(function(){
        $('.validation-form').remove();
    }, 5000);
  } else {
    $('form#fale').append('<p class="-white -small m-0 response-form">Enviando dados, aguarde...</p>');

    $.ajax({
      type: "POST",
      url: $('form#fale').attr('action'),
      data: $('form#fale').serialize(),
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      success: function (result) {
        $('.response-form').remove();
        $('form#fale').append('<p class="-white -small m-0 response-form view">Formulário enviado com sucesso!</p>');
        setTimeout(function(){
            $('.response-form').remove();
        }, 5000);
        //window.location.reload();
      },
      error: function (xhr, ajaxOptions, thrownError) {
        $('.response-form').remove();
        $('form#fale').append('<p class="-white -small m-0 response-form view">Ocorreu um erro ao enviar sua mensagem.</p>');
        setTimeout(function(){
            $('.response-form').remove();
        }, 5000);
      }
    });
  }
});
</script>

<style>
section  ul {
  margin-left: 3em!important;
  margin-bottom:35px;
  list-style:none;
}
section  ol {
  margin-left: 3.5em!important;
  margin-bottom:0px;
}

section ul li::before{
 background-color:#f37020;
}

section ul li::before {
  content: '';
  display: inline-table;
  vertical-align: middle;
  width: 4px;
  height: 21px;
  background-color: #f37020;
  border-bottom-left-radius: 25px;
  border-top-right-radius: 25px;
  -webkit-transform: rotateZ(30deg);
  transform: rotateZ(30deg);
  margin-right: 15px;
  margin-left: 0px;
  top: -.2rem;
  position: relative;
}

section li {
  margin-bottom:0px;
}

#contact .contato-link {
  line-height:1.5!important;
}
</style>
@include('inc.footer')
