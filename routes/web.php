<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */
Route::get('/', 'HomeController@index')->name('site.home');

// Route::get('/midiamalls', 'MidiaController@index')->name('midia');

// Route::get('/propriedades', 'PropriedadesController@index')->name('propriedades');

Route::get('/nossos-shoppings', 'ShoppingsController@index')->name('nossos-shoppings');

Route::put('/fale-conosco', 'HomeController@faleconosco')->name('midia.faleConosco');

Route::post('logout', 'Auth\LoginController@logout');

//ROTAS DE ADMIN
Route::group(['prefix' => 'admin'], function () {
    Auth::routes();
    Route::get('/', 'AdminController@index')->name('admin.home');
    Route::get('/home', 'AdminController@index')->name('admin.home');

    Route::group(['prefix' => 'footer'], function () {

        Route::get('/', 'admin\FooterController@index')->name('admin.footer');
        Route::put('/footerupdate', 'admin\FooterController@update')->name('admin.footer.update');
        Route::put('/delete', 'admin\FooterController@delete')->name('admin.footer.delete');
        Route::put('/insert', 'admin\FooterController@insert')->name('admin.footer.insert');

    });

    Route::group(['prefix' => 'home'], function () {

        Route::get('/', 'admin\HomeController@index')->name('admin.home');
        Route::put('/update', 'admin\HomeController@update')->name('admin.home.update');
        Route::put('/delete', 'admin\HomeController@delete')->name('admin.home.delete');
        Route::put('/insert', 'admin\HomeController@insert')->name('admin.home.insert');
        Route::get('/deletepreco', 'admin\HomeController@deletepreco')->name('admin.home.deletepreco');

    });

    Route::group(['prefix' => 'midia'], function () {

        Route::get('/', 'admin\MidiaController@index')->name('admin.midia');
        Route::put('/update', 'admin\MidiaController@update')->name('admin.midia.update');
        Route::put('/delete', 'admin\MidiaController@delete')->name('admin.midia.delete');
        Route::put('/insert', 'admin\MidiaController@insert')->name('admin.midia.insert');

    });

    Route::group(['prefix' => 'propriedades'], function () {

        Route::get('/', 'admin\PropriedadesController@index')->name('admin.propriedades');
        Route::get('/novo', 'admin\PropriedadesController@novo')->name('admin.propriedades.new');
        Route::put('/novo/inserir', 'admin\PropriedadesController@inserirnovo')->name('admin.propriedades.insert');
        Route::get('/{slug}', 'admin\PropriedadesController@editarpropriedade')->name('admin.propriedades-edit');
        Route::put('/item/update', 'admin\PropriedadesController@updateprop')->name('admin.propriedades-update');

        Route::put('/update', 'admin\PropriedadesController@update')->name('admin.propriedades.update');
        Route::put('/delete', 'admin\PropriedadesController@delete')->name('admin.propriedades.delete');

    });

    Route::group(['prefix' => 'nossos-shoppings'], function () {

        Route::get('/', 'admin\ShoppingsController@index')->name('admin.shoppings');
        Route::get('/novo', 'admin\ShoppingsController@novoshopping')->name('admin.shoppings.new');
        Route::put('novo/inserir', 'admin\ShoppingsController@inserirnovo')->name('admin.shoppings.novo.insert');
        Route::put('/shopping/update', 'admin\ShoppingsController@editshopping')->name('admin.shoppings.edit');
        Route::put('/shopping/delete', 'admin\ShoppingsController@deleteshopping')->name('admin.shoppings.delete');
        Route::put('/shopping/deletemidia', 'admin\ShoppingsController@deletemidia')->name('admin.shoppings.deletemidia');
        Route::get('/shopping/deletepreco', 'admin\ShoppingsController@deletepreco')->name('admin.shoppings.deletepreco');
        Route::get('/shopping/deletemidiageral', 'admin\ShoppingsController@deletemidiageral')->name('admin.shoppings.deletemidiageral');

        Route::put('/update', 'admin\ShoppingsController@update')->name('admin.shoppings.update');
        Route::put('/insert', 'admin\ShoppingsController@insert')->name('admin.shoppings.insert');

    });

    Route::group(['prefix' => 'fale-conosco'], function () {

        Route::get('/', 'admin\FaleController@index')->name('admin.fale');
        Route::get('/export', 'admin\FaleController@export')->name('admin.fale.export');
        Route::put('/update', 'admin\FaleController@update')->name('admin.fale.update');
        Route::put('/delete', 'admin\FaleController@delete')->name('admin.fale.delete');
        Route::put('/insert', 'admin\FaleController@insert')->name('admin.fale.insert');

    });

    //USERS
    Route::get('users', ['as' => 'admin.users', 'uses' => 'admin\UsersController@index']);
    Route::get('users/create', ['as' => 'admin.users.create', 'uses' => 'admin\UsersController@create']);
    Route::get('users/change-password', ['as' => 'admin.users.changePassword', 'uses' => 'admin\UsersController@changePassword']);
    Route::get('users/edit/{id}', ['as' => 'admin.users.edit', 'uses' => 'admin\UsersController@edit']);
    Route::put('users/destroy/{id}', ['as' => 'admin.users.destroy', 'uses' => 'admin\UsersController@destroy']);
    Route::post('users/store/{id?}', ['as' => 'admin.users.store', 'uses' => 'admin\UsersController@store']);
    Route::post('users/storePassword/{id?}', ['as' => 'admin.users.storePassword', 'uses' => 'admin\UsersController@storePassword']);

});
